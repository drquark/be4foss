The *Blauer Engel Für FOSS* project was funded by the Federal German Environment Agency (UBA) and the Federal Ministry for the Environment, Nature Conservation, Nuclear Safety and Consumer Protection (BMUV). The funds are made available by resolution of the German Bundestag.

![Logo of the Federal German Environment Agency.](images/sec4_uba.jpg)

![Logo of the Federal Ministry for the Environment, Nature Conservation, Nuclear Safety and Consumer Protection.](images/sec4_bmuv.png)

The publisher is responsible for the content of this publication.
