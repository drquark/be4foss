# PART II: Eco-Certifying Desktop Software

![Okular, KDE’s popular multi-platform PDF reader and universal document viewer, was awarded the Blue Angel ecolabel in March 2022 (image published under a [CC-BY-4.0](https://spdx.org/licenses/CC-BY-4.0.html) license).](images/sec2_okular-BE-logo.png)

What do construction products, toilet paper, and software have in common?

Each of these  can be eco-certified by the Blue Angel environmental label&mdash;the official environmental label of the German government!

The Blue Angel ecolabel is awarded to a range of products and services, from paper products and construction materials to printers, and certifies that the product meets a list of stringent requirements for being environmentally friendly over a product's life cycle. In 2020, the German Environment Agency (UBA) extended the award criteria to include software products, which was the first environmental certification in the world to link transparency and user autonomy with sustainability.

Specifically, eco-certification requires being transparent about the energy consumption when using the software, and ensuring that the software is capable of running on older hardware. Moreover, the criteria also include a list of requirements related to user autonomy which reduce the environmental impact of software.

![The three steps to eco-certification: Measure, Analyze, Certify (image from Karanjot Singh published under a [CC-BY-SA-4.0](https://spdx.org/licenses/CC-BY-SA-4.0.html) license).](images/sec2_3StepsToBEECO.png)

This manual will provide a broad overview of the Blue Angel and the ABCs of the award criteria for desktop software. It will also demonstrate how meeting the award criteria can reduce environmental harm&mdash;and hopefully motivate you and your team to eco-certify your software project, too! Here, there will be a focus on the user autonomy requirements of the Blue Angel award criteria, which we will return to in Part III. But first, a brief introduction to the Blue Angel and the KDE Eco initiative. 

## The Blue Angel

Introduced in 1978, the Blue Angel is the oldest ecolabel in the world and the official environmental label awarded by the German government. The label is adminstered by Germany's Federal Ministry for the Environment, Nature Conservation, Nuclear Safety, and Consumer Protection (German: *Bundesministerium für Umwelt, Naturschutz, nukleare Sicherheit und Verbraucherschutz*, or BMUV). The Blue Angel ecolabel is also a member of the [Global Ecolabelling Network (GEN)](https://globalecolabelling.net/), an international network of Type I ecolabels which [at the time of writing](https://web.archive.org/web/20221026003053/https://globalecolabelling.net/) has 37 members across nearly 60 countries. 

![Logo of the Blue Angel ecolabel. The logo is intentionally designed to correspond to the logo of the United Nations Environment Programme. This reflects the aim of the German government to embed the UNEP goals in Germany. (Image published under a [CC-BY-SA-4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.de) license.)](images/sec2_blauer-engel-logo.png)

The Blue Angel was not the first Type I ecolabel for software&mdash;the Hong Kong Green Council, also a member of the Global Ecolabelling Network, released [criteria in 2010](https://greencouncil.net/hkgls/GL006004_rev0.pdf) for Green IT software. But the Blue Angel ecolabel criteria are the first to identify a process for measuring software's energy consumption, and the first to specify ways that user independence reduces environmental harm. 

'What is a Type I environmental label?', you may be wondering. For Type I labels, the entire life cycle of the product is taken into account, and compliance with the award criteria is assessed by a third-party (compliance with Type II environmental labels, on the other hand, are self-declared and do not require any third-party auditing.) 

The Blue Angel ecolabel is currently awarded to around 100 product groups and services across a variety of sectors, including paper products, building products, furnishings, clothing, washing and cleaning agents, cleaning services, household chemicals, packaging, vehicles, energy and heating, and household electrical devices. As of 2022, with the eco-certification of KDE's popular PDF and universal document reader, that list also includes desktop software.

The award criteria for certification are developed transparently by the German Environment Agency (UBA), and the process includes the Environmental Label Jury, a body made up of suppliers as well as civil society organizations and research institutions. The independent auditor RAL gGmbH assesses compliance with the award criteria and awards the seal. Importantly, the Blue Angel does not certify that a product is completely harmless. Instead, certified products represent a "lesser evil" with respect to environmental harm&mdash;this can be summed up with the motto '*as little as possible, as much as necessary*'. Rather than compare different products, the Blue Angel ecolabel indicates that a product fulfills a list of requirements for a specific category.

## The ABCs Of The Award Criteria For Desktop Software

The Blue Angel's award criteria for ["Resource and Energy-Efficient Software Products"](https://www.blauer-engel.de/en/products/electric-devices/resources-and-energy-efficient-software-products) were released in January 2020. As stated in the criteria, there are two primary objectives of the Blue Angel for software: (i) to award software with lower performance requirements such that "longer operating lives for [...] hardware are possible"; and (ii) to recognize products which "stand out due to their high level of transparency and give users greater freedom in their use of the software" (p. 6). To achieve this, there are three main categories of the criteria, referred to here as the ABCs of the award criteria.

 - (A) Resource & Energy Efficiency

 - (B) Potential Hardware Operating Life

 - (C) User Autonomy

The criteria listed in category (A) not only requires that the energy consumption of a software product be measured and reported, but states that the energy consumption of the software canoot increase by more than 10% from the time of certification. Energy consumption data is measured using an external power meter, and other hardware performance information such as CPU usage, network traffic, etc., while running the software in a representative way.

The criteria in category (B) ensure that the software has low-enough performance requirements to run on older, less powerful hardware. Compliance entails a declaration of backward compatibility, with details about the hardware and the required software stack at least five years old on which the software runs.

Finally, the criteria in category (C) ensure that users have an influence on the energy consumption and resource-conserving use of their software. There are eight categories for the autonomy criteria: 

 1. Data Formats &mdash; *Interoperability To Give Users A Choice To Use Other, More Efficient Software*

    Data formats should not be used by vendors to [lock in](https://en.wikipedia.org/wiki/Vendor_lock-in) users to a specific computer program, nor should they impose onerous switching costs. [Interoperable](https://en.wikipedia.org/wiki/Interoperability) data formats prevent users from being stuck using a program that consumes a high amount of energy, when a more efficient one can achieve the same results. Users should also be able to easily change programs and still access all of their data.

 2. Transparency &mdash; *Being Open To Remove Dependencies, Essential For Long-Term Use*
 
    Transparency in software code and application interfaces removes dependencies on a particular company or organization as well as restrictions on the short and long-term use software. When developers decide to end support for their software, either continued security updates should be provided (see below) or the source code should be made publicly available so third parties can continue support for the software. Furthermore, enhancing the functionality of software must not be limited by restrictive or undocumented application interfaces (APIs). Indeed, issues related to application interfaces are so critical they even came before the United States Supreme Court in 2021, where there was a [favorable ruling for fair use](https://en.wikipedia.org/wiki/Oracle_America,_Inc._v._Google,_Inc.#Decision) of transparent interfaces.

 3. Continuity Of Support &mdash; *Security Updates To Prevent E-Waste*
 
    Dependency on suppliers for essential updates should not leave users with [abandonware](https://en.wikipedia.org/wiki/Abandonware), where the software (and thus, hardware) cannot be used without serious disadvantages such as vulnerabilities to [malware](https://en.wikipedia.org/wiki/Malware). Security updates should be provided for up to five years after discontinued development. Moreover, security updates should be separable from functional updates so users are not coerced into adopting unwanted functionalities from [feature creep](https://en.wikipedia.org/wiki/Feature_creep) and other forms of [software bloat](https://en.wikipedia.org/wiki/Software_bloat).

 4. Uninstallability &mdash; *Removing Unwanted Software To Increase Efficiency*

    Being able to completely uninstall software that is not needed has ecological benefits. Similar to software bloat and feature creep, unwanted software or software components can create inefficiencies by occupying memory, wasting processing time, adding disk usage, consuming storage, and causing delays at system startup and shutdown. When a user no longer wishes to continue using a software product, it must be possible to completely purge it from the system, while keeping all user-generated data.

 5. Offline Capability &mdash; *Removing Dependencies To Keep Software Usable Long-Term And Decrease Energy Consumption*
 
    Use of the software should be possible without an internet connection&mdash;unless, of course, a network connection is necessary for the software's intended functionality. License servers and other forms of access control restrict use of an application in ways unnecessary to the software's designed functionality. When a server goes down or there is Internet outage, such access control locks people out from using their software, [possibly permanently](https://en.wikipedia.org/wiki/Abandonware#Implications). Moreover, such dependencies use network traffic, and thus consume energy beyond that needed for the intended functionality of the software.

 6. Modularity &mdash; *Having Essential Functions Only To Decrease Memory And Energy Demands*

    Users should be able to install only what they need. Non-essential functions increase memory and energy demands, making the software less efficient and perhaps unable to run on older hardware. People should have the ability to limit the range of software functions to those that they either want or require.

 7. Freedom From Advertising &mdash; *Opting-Out To Reduce Energy Consumption*
 
    As discussed in Part I, unwanted data use in the European Union alone is roughly equivalent to the annual energy consumption of a city like Lisbon or Turin. Allowing users to opt out of advertizing reduces resource and energy demands on end-user devices as well as on the servers pushing the ads. Opting out also decreases data volume transmitted and thus energy consumption from network traffic.

 8. Documentation &mdash; *To Support Resource-Conserving, Continuous Use Of Software, And Therefore Hardware*
 
    Documentation is a prerequisite for long-term viability of a software product. Documentation is also necessary for the resource-conserving use of an application. By documenting the above criteria, users can continue using the software and thus hardware in a sustainable way and developers can continue to maintain the software without dependencies on or restrictions from vendors.

Software products that comply with the award criteria are less likely to suffer from bloat and other inefficiencies. This mitigates problems related to software-driven hardware obsolescence and can reduce e-waste, which in turn results in fewer devices needing to be produced and shipped, and valuable metals do not need to be mined and processed. By ensuring user autonomy, developers can make sure that the software they produce has a lighter environmental impact&mdash;whether through keeping devices in use for longer, or reducing their resource and energy consumption when in use. 

The Blue Angel award criteria for software, with its focus on transparency in resource and energy-efficiency, hardware operating life, and user autonomy, provide an excellent benchmark to begin a discussion on software sustainability and push development in this area forward. Moreover, in FOSS communities we often take user autonomy and transparency and their benefits for granted. Although being Free & Open Source Software is not a requirement to obtain the Blue Angel ecolabel, it is in this category that FOSS really shines&mdash;in so many ways, we are already at the forefront of sustainable software design!

## <a name="sec:okular"></a> First Eco-Certified Computer Program: KDE's Popular Document Reader Okular

[Okular](https://okular.kde.org/), KDE’s popular multi-platform PDF reader and universal document viewer, was the first software product to be officially recognized for sustainable software design as reflected in the Blue Angel award criteria. Introduced in 1978, the Blue Angel is the world’s earliest-established environmental label. In February 2022, Okular was the first-ever software product to be certified with its seal! What is more, Okular is the first eco-certified computer program within the Global Ecolabelling Network representing nearly 60 countries. 

In 2021, KDE started KDE Eco, a project with the goal of putting KDE and Free Software at the forefront of sustainable software design. Sustainability is not a new for Free & Open Source Software (FOSS)&mdash;the [four freedoms](https://fsfe.org/freesoftware/index.en.html) have always put Free Software at the forefront of sustainable software design. What is new is that the two pillars of FOSS, namely, transparency and user autonomy, are now directly linked to sustainability goals by an organization like the German Environment Agency (UBA).

![Logo of the KDE Eco initiative (image published under a [CC-BY-4.0](https://spdx.org/licenses/CC-BY-4.0.html) license).](images/sec2_kde-eco-logo-domain.png)

The Blue Angel award criteria reflect KDE’s values and those of the larger FOSS movement seamlessly. With Free & Open Source Software, transparency is guaranteed and control is handed over to users, instead of being held back by vendors or service providers. This allows users to decide what they want from the software they use and, all too often overlooked, the hardware as well. For instance, users may drive down the energy consumption of their programs with no loss in functionality as they can install only what they need, no more and no less; and avoid advertising and data-mining options which run processes in the background consuming resources. As for FOSS developers, they typically continue to support hardware that the industry would be eager to make obsolete, providing users with up-to-date and secure software for devices that might otherwise be discarded as e-waste and end up polluting landfills.

![Okular's energy consumption report.](images/sec2_okular-energy-consumption.png)

KDE was founded in 1996, and has become a world-wide community of software engineers, artists, writers, translators, and creators who are committed to Free Software development. KDE maintains numerous FOSS products, including the Plasma desktop environment; the design app for painters and graphic artists, Krita; the GCompris suite of educational activities for children; Kdenlive, a professional video-editing software product; and, of course, Okular, with which you can view all sorts of documents, including PDFs, comics, scientific and academic papers, and technical drawings. With KDE’s long-standing mission and guiding vision, as well as the talent and capabilities of its community members, it is not surprising that KDE is a pioneer in championing sustainable software. Now, with the first ever eco-certified software product, the KDE community is [celebrating the achievement](https://eco.kde.org/blog/2022-09-28_okular_blue-angel-award-ceremony/) together with the [wider](https://www.linux-magazin.de/news/pdf-reader-okular-erhaelt-blauen-engel/) [Free Software](https://fsfe.org/news/2022/news-20220316-01.de.html) [community](https://netzpolitik.org/2022/nachhaltigkeit-erste-software-mit-blauem-engel-ausgezeichnet), as well as with the computer science department at [Umwelt Campus Birkenfeld](https://www.umwelt-campus.de/en/forschung/projekte/green-software-engineering/news-details/first-blue-angel-for-software), where researchers measured the resource and energy-consumption of Okular and other KDE software.

Released under the GPLv2+ license, Okular is FOSS and so was also already fulfilling many of the user autonomy criteria necessary to obtain the Blue Angel seal of approval. Further work was carried out to make Okular fully compliant with all of the award criteria in order to become officially recognized as providing transparency in energy and resource consumption, extending the potential hardware operating life of devices, and enabling user autonomy.

![Icon for KDE's popular application Okular (image published under a [???]() license).](images/sec2_okular.svg)

Okular works on Linux, Windows, Android, and Plasma Mobile, and is available to download for all GNU/Linux distributions, as a standalone package from Flathub and the Snap Store, through the KDE F-Droid release repository for Android, as well as from the Microsoft Store. Okular also lets you check digital signatures and sign documents yourself, as well as include annotated text and comments directly embedded into the document.Being FOSS, the source code is also readily available at Okular’s GitLab repository for all to use, study, share, improve, and most of all, enjoy.

KDE and the Free Software community would like to send a heartfelt thank you to the Okular developers for making environmentally-friendly software for all of us! In the next section we will look at the steps you need to do to join us in having your software project recognized for sustainable software design.

## Part II Sources

Some material in this section is based directly on the following three texts:

 - The Wikipedia article <a href="https://en.wikipedia.org/wiki/Blue_Angel_(certification)">"Blue Angel (certification)"</a>, which is released under the <a href="https://invent.kde.org/teams/eco/be4foss/-/blob/master/LICENSES/CC-BY-SA-3.0.txt">Creative Commons Attribution-Share-Alike License 3.0</a>.
 
 - The Wikipedia article "<a href="https://en.wikipedia.org/wiki/Software_bloat">Software bloat</a>", which is released under the <a href="https://invent.kde.org/teams/eco/be4foss/-/blob/master/LICENSES/CC-BY-SA-3.0.txt">Creative Commons Attribution-Share-Alike License 3.0</a>.

 - The KDE Eco blog post <a href="https://eco.kde.org/blog/2022-03-16-press-release-okular-blue-angel/">"First Ever Eco-Certified Computer Program: KDE's Popular PDF Reader Okular"</a>, which is released under the <a href="https://invent.kde.org/teams/eco/be4foss/-/blob/master/LICENSES/CC-BY-SA-4.0.txt">Creative Commons Attribution-Share-Alike License 4.0</a>.