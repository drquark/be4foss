# PART III: Fulfilling The Blue Angel Award Criteria

![Monitoring energy and hardware consumption in real time with KDE's LabPlot (image published under a [CC-BY-NC-ND-4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/) license).](images/sec3_labplot_live-measurement.png)

The three main categories of the Blue Angel award criteria for desktop software are:

 - (A) Resource & Energy Efficiency

 - (B) Potential Hardware Operating Life

 - (C) User Autonomy

In this section I'll cover the three categories in more depth, providing a hands-on guide to fulfilling each set of criteria. There are numerous benefits of going through the certification process. By making the energy consumption of your software transparent and complying with the hardware operating life and user autonomy criteria, you get the benefits of:

 - **Eco-Certification**: Apply for the Blue Angel ecolabel to demonstrate to users, companies, and governmental organizations that your software is designed sustainably.

 - **Data-Driven Development**: Locate areas of inefficiencies in terms of energy consumption, and make data-driven decisions for your software development.

 - **Sustainable Software Design**: Take user autonomy criteria into consideration when planning your software design.
 
 - **Providing End-User Information**: Highlight to your users the ways your software is already sustainably designed by using the Blue Angel criteria as a benchmark.

## (A) How To Measure Your Software

The lab setup consists of a power meter, a computer to aggregate and evaluate the power meter output, and a desktop computer for the system under test where user behavior is emulated. The setup described here follows the specifications from the [Blue Angel Basic Award Criteria for Resource and Energy-Efficient Software Products](https://produktinfo.blauer-engel.de/uploads/criteriafile/en/DE-UZ%20215-202001-en-Criteria-2020-02-13.pdf). Terminology comes in part from Kern et al. (2018): ["Sustainable software products &mdash; Towards assessment criteria for resource and energy efficiency"](https://doi.org/10.1016/j.future.2018.02.044).

### Overview Of Lab Setup

The laboratory setup requires 1 power meter and at least 2 computers:

 - *Power Meter*

    One of the devices recommended by the Blue Angel is the [Gude Expert Power Control 1202 Series](https://www.gude.info/en/power-distribution/switched-metered-pdu/expert-power-control-1202-series.html) ([manual](https://gude-systems.com/app/uploads/2022/05/manual-epc1202-series.pdf)). It provides plugs for powering the computer and measures the current during operation. The device can be controlled and read via cabled Ethernet. There is a web-based user interface, a [REST API](http://wiki.gude.info/EPC_HTTP_Interface), and the device supports various protocols such as SNMP or syslog.

 - *Computer 1: Data Aggregator & Evaluator*

    The computer for collecting and evaluating results from the power meter.

    A Python script to read out the data from the Gude Expert Power Control 1202 Series is available at the [FEEP repository](https://invent.kde.org/teams/eco/feep/-/tree/master/tools/GUDEPowerMeter).
    
    It is recommended to monitor progress live with the second computer in order to ensure everything is proceeding smoothly. This can be done with KDE's [Labplot](https://apps.kde.org/labplot2/), for instance; see [here](#sec:labplot).

    Other power meters may require non-Free software, e.g., Janitza's [GridVis Power Grid Monitoring Software](https://www.gridvis.com/gridvis-overview.html).

![Gude Power Meter (image published under a [XXX]() license).](images/sec3_gude-pm.jpg)

  - *Computer 2: System Under Test*

    The reference system is the hardware used to measure the energy consumption of the system under test, or SUT. The SUT includes the operating system and software installed for (i) testing the software product, (ii) emulating the standard usage scenario[^2] and (iii) collecting the hardware performance results.

[^2]: It is also possible to have a setup using 3 computers, with the Standard Usage Scenario emulation generated on a computer independent of the SUT; see Kern et al. (2018). Details of a similar [setup for an external workload generator](https://invent.kde.org/teams/eco/feep/-/tree/master/tools/arduino-board_external-workload-generator.md) can be found at the FEEP repository.
 
Note the following:

For GNU/Linux systems, the [Blue Angel criteria (Section 1.1)](https://produktinfo.blauer-engel.de/uploads/criteriafile/en/DE-UZ%20215-202001-en-Criteria-2020-02-13.pdf) recommend Fujitsu computers as the reference system.

For emulating activity in the standard usage scenario, Free Software task automation tools such as [`xdotool`](https://github.com/jordansissel/xdotool), [`KDE Eco Tester` (in progress)](https://invent.kde.org/teams/eco/feep/-/tree/master/tools/KdeEcoTest), or [`Actiona`](https://wiki.actiona.tools/doku.php?id=en:start) (GPLv3) can be used.

For collecting hardware performance data (e.g., processor and RAM utilization, hard disk activity, network traffic), a Free Software tool such as [`Collectl`](https://sourceforge.net/projects/collectl/) (GPLv2/Artistic License) can be used.

It's also possible to [repurpose cheap switchable power plugs as measurement devices](https://volkerkrause.eu/2020/10/17/kde-cheap-power-measurement-tools.html); see Section ["Alternative: Gosund SP111 Setup"](#sec:gosundPM) for set up instructions.

![Gude Power Meter (Image published under a [XXX]() license)](images/sec3_lab-setup_modified.png)

#### System Under Test (SUT)

The Fujitsu Esprimo P920 Desktop-PC proGreen selection (Intel Core i5-4570 3,6GHz, 4GB RAM, 500GB HDD) is one of the recommended reference systems.

On the reference system you can set up the System Under Test (SUT). The SUT must reduce unrelated energy consumption and have a standardized configuration. This includes the following:

 - Overwriting the entire hard drive of the machine with a standardized OS.

 - Deactivating all possible background processes (automatic updates, backups, indexing, etc.).

 - Installing the necessary software (i.e., the application under consideration as well as the emulation and data collection software).

#### Standard Usage Scenario (SUS)

Preparing the SUS requires the following:

 - Identifying tasks users typically carry out when using the application under consideration.

 - Identifying functionalities which require high energy demand or high resource utilization.

 - Based on the above, scheduling a flow chart of individual actions and emulating these actions with a task automation tool.

 - Note that when running the SUS, the cache should be cleared between runs and any new files deleted before starting the next measurement.

![Steps for preparing Standard Usage Scenario (SUS) scripts to measure the energy consumption of software (image from Karanjot Singh published under a [CC-BY-SA-4.0](https://spdx.org/licenses/CC-BY-SA-4.0.html) license).](images/sec3_PreparingSUS.png)

An example of a standard usage scenario for KMail includes: searching for an email, writing a reply or forwarding the email, saving an attachment, deleting a folder in the mail client, etc. See the [Actiona scripts](https://invent.kde.org/teams/eco/feep/-/tree/master/measurements) used to test KMail, Krita, and Okular for examples.

Note: If the task automation tool uses pixel coordinates to store the position of the automated clicks (e.g., `Actiona`) and, moreover, the screen resolution of the computer used in preparation differs from that of the laboratory computer, all pixel coordinates will eventually have to be set anew for the laboratory environment (Seiwert & Zaczyk 2021: p. 12).

##### Emulation Tools For Usage Scenarios

An automation tool is required which can run the usage scenarios and does not need human intervention, so it can be run repeatedly in a well-defined way to provide accurate measurements.

Beyond [`xdotool`](https://github.com/jordansissel/xdotool), [`KDE Eco Tester` (in progress)](https://invent.kde.org/teams/eco/feep/-/tree/master/tools/KdeEcoTest), or [`Actiona`](https://github.com/Jmgr/actiona), there are other candidates for tools which might meet the requirements. See a list from KDE Contributor David Hurka in the presentation ["Visual Workflow Automation Tools"](https://invent.kde.org/teams/eco/feep/-/tree/master/tools/presentation_Visual_Workflow_Automation_Tools).

Most of those tools use X11-specific features, and thus do not work on Wayland systems. There are a few possible approaches here:

 - [Selenium Webdriver using AT-SPI](https://invent.kde.org/sdk/selenium-webdriver-at-spi) 

 - [The XDG RemoteDesktop portal](https://docs.flatpak.org/en/latest/portal-api-reference.html#gdbus-org.freedesktop.portal.RemoteDesktop)

 - Various Wayland protocols (support varies between compositors):
    - https://github.com/swaywm/wlr-protocols/blob/master/unstable/wlr-virtual-pointer-unstable-v1.xml  
    - https://api.kde.org/frameworks/kwayland/html/classKWayland_1_1Client_1_1FakeInput.html

 - [libinput user devices](https://lwn.net/Articles/801767/)

### Measuring Software

The measurement process is defined in Appendix A of the [Basic Award Criteria](https://produktinfo.blauer-engel.de/uploads/criteriafile/en/DE-UZ%20215-202001-en-Criteria-2020-02-13.pdf). It requires recording and logging energy data and performance indicators with a granularity of 1 second so that they can be processed and average values can be calculated.

Some general comments:

 - Times between the PM and Computer 1 (Data Aggregator & Evaluator) must be synchronized.

 - On Computer 1, confirm that the desired power outlet is read out (e.g., via the live graph created when using `LabPlot`).

 - When using `Collectl` to collect performance load, ensure it is running in the console of the SUT; also, check that the required CSV file is also correctly generated.

 - Since each run of the usage scenarios results in changes to the standard operating system, clearing the cache between runs is recommended.

 - All runs (Baseline, Idle Mode, Standard Usage Scenario) must be for the same length of time, based on the time needed to run the usage scenario script.

During the energy measurement, you also need to record a set of performance indicators: processor utilisation, RAM utilisation, hard disk activity and network traffic. Tool candidate:

 - [`Collectl`](https://sourceforge.net/projects/collectl/)

The following command is used for obtaining hardware performance data with `Collectl` (Mai 2021: p. 15):

`$ collectl -s cdmn -i1 -P --sep 59 -f ~/performanceMeasures.csv`

The specified options are:

 - `-s cdmn`
 
    collect CDU, Disk, memory, and network data

 - `-i1`

    sampling interval of 1 second

 - `-P`

    output in plot format (separated data which consists of a header with one line per sampling interval)

 - `--sep 59`

    semicolon separator for -P option

 - `-f /PATH/TO/FILE.csv`

    save file at specified path

#### Baseline, Idle Mode, And Standard Usage Scenario

 -  Baseline: *Operating System* (OS)

    To establish the baseline energy consumption and hardware performance data of the system under test, a scenario is measured in which the OS is booted and running but no action is taken.

 -  Idle Mode: *OS + Application While Idle*

    To establish the energy consumption and hardware performance data of the application while idle, a scenario is measured in which the application under consideration is opened but no action is taken.

Important: the baseline and idle mode are run for the same time needed to carry out the standard usage scenario. Since the power consumption for the baseline and idle scenario is relatively uniform, 10 repetitions for each is considered sufficient to obtain a representative sample (Seiwert & Zaczyk 2021).

 -  Standard Usage Scenario: *OS + Application In Use*

    To measure the energy consumption and hardware performance data of the application under consideration in use, the standard usage scenario is run. The measurement of the standard usage scenario should be repeated 30 times, taking several hours in total. This higher number of repetitions is necessary to obtain a representative sample, as the energy consumption and performance data may vary across measurements (Seiwert and Zaczyk 2021).

#### <a name="sec:labplot"></a> Monitoring Output With Labplot

You can use KDE's [LabPlot](https://apps.kde.org/labplot2/) to monitor the output live as data is coming in. To do so:

 - Redirect the power meter output to a CSV file.

 - In LabPlot, import the CSV file by going to `File > Add New > Live Data Source ....`

 - Where it says "Filter", select the Custom option. Under "Data Format" define the separator value used (e.g., comma, semi-colon, space).

 - You can check that the output is correct under the "Preview" tab.
 
 - If everything looks good, click OK.
 
 - Now it is just a matter of right-clicking on the data frame window and selecting `Plot Data > xy-Curve`.

### Analysis Of The Results With OSCAR

There is a tool available from Umwelt Campus Birkenfeld which generates reports from measurement data, called `OSCAR` (Open Source Software Consumption Analysis):

  - [Source Code](https://gitlab.umwelt-campus.de/y.becker/oscar-public)

  - [Running instance](https://oscar.umwelt-campus.de/)

At the project website, you can also find the Oscar Manual with detailed instructions, including screenshots on how to use `OSCAR`.

#### CSV Files 

Analysis with `OSCAR` requires uploading the following files to the [OSCAR website](https://oscar.umwelt-campus.de/): (i) a log file of actions taken, (ii) the energy consumption data, and (iii) the hardware performance data. All files are CSV files, examples of which are provided below. Important: `OSCAR` can be particular about the data formats, some preprocessing of the raw data may be necessary (e.g., performance data measured by `Collectl`; see below for details).

To test OSCAR yourself, you can download the data for Okular for the baseline and SUS measurements here: <TODO: ADD LINK>. The data has been used to generate a report successfully using fOSCAR v0.190404, which you can download here: <TODO: ADD LINK>.

 - Log File Of Actions

An example log file of actions for measuring KDE's text editor (and more) [`Kate`](https://apps.kde.org/kate/). Here the iteration number, timestamp in date-time format, and actions are listed in three columns; note that the start and end of each iteration must be labelled with 'startTestrun' and 'stopTestrun', respectively.

|             |                     |                               |
|-------------|---------------------|-------------------------------|
| iteration 1 | 2022-05-21 18:54:36 | startTestrun                  |
| iteration 1 | 2022-05-21 18:55:41 | go to line 100                |
| iteration 1 | 2022-05-21 18:55:46 | toggle comment                |
| iteration 1 | 2022-05-21 18:55:50 | find kconfig                  |
| iteration 1 | 2022-05-21 18:55:55 | move between searches 6 times |
| iteration 1 | 2022-05-21 18:56:05 | close find bar                |
| iteration 1 | 2022-05-21 18:56:05 | standby 30 sec                |
| iteration 1 | 2022-05-21 18:56:35 | go to line 200                |
| iteration 1 | 2022-05-21 18:56:40 | select 10 lines               |
| iteration 1 | 2022-05-21 18:56:43 | delete selected text          |
 
 -  Energy Consumption Data

The energy consumption data will have the following format, in which (i) the timestamp increases in one-second increments and (ii) *value* stands for 'averaged measured value per second in watts'.:

|            |        |
|------------|--------|
| timestamp; | value1 |
| timestamp; | value2 |
| timestamp; | value3 |
| timestamp; | value4 |

Below is an example of the raw output for 7 rows from the Gude Power Meter. The first column shows the timestamp in nanoseconds in [Epoch time](https://en.wikipedia.org/wiki/Epoch_time), and the second column Watts consumed. 

|                  |    |
|------------------|----|
| 1661611923019071 | 43 |
| 1661611923142924 | 43 |
| 1661611924293989 | 29 |
| 1661611924417017 | 28 |
| 1661611924744885 | 28 |
| 1661611924869051 | 28 |
| 1661611924992392 | 28 |

The raw data can be preprocessed in R: nanoseconds in Epoch time are converted to human-readable date-time forms with the command `as.POSIXct(<NANOSECONDS>/1000000, origin = '1970-01-01', tz = 'Europe/Berlin')`, and Watts are averaged per second. The same data above is shown below after preprocessing, with *n* indicating the number of values used to calculate the average:

| datetime            | n | watts    |
|---------------------|---|----------|
| 2022-08-27 16:52:03 | 2 | 43.00000 |	
| 2022-08-27 16:52:04 | 5 | 28.20000 |

Below is another example from the OSCAR Manual. Here, measurements are from the Janitza UMG 604 power meter exported on the DAE using the GridVis interface. The columns above containing the min and max values are ignored by OSCAR.

| Row-Number; | Time; | Valueavg[W]; | Value-min[W]; | Value-max[W]; |
|-------------|-------|--------------|---------------|---------------|
| 1; | 18.01.12 17:00:01; | 78,573; | 78,281; | 78,982; |
| 2; | 18.01.12 17:00:02; | 78,611; | 78,390; | 78,972; |
| 3; | 18.01.12 17:00:03; | 78,556; | 78,353; | 78,887; |
| 4; | 18.01.12 17:00:04; | 78,589; | 78,391; | 78,882; |
| 5; | 18.01.12 17:00:05; | 78,546; | 78,288; | 79,057; |
| 6; | 18.01.12 17:00:06; | 78,580; | 78,252; | 78,941; |
| 7; | 18.01.12 17:00:07; | 80,712; | 78,330; | 87,391; |
| 8; | 18.01.12 17:00:08; | 78,545; | 78,275; | 79,202; |

 - Performance Data (Raw)

When using `Collectl` for collecting hardware performance data it is necessary to do the following before uploading the data to OSCAR (see Seiwert & Zaczyk 2021: p. 13 for details; see also Appendix A 2 on p. 46 for a Python script to automate some of these tasks):

 - All information above the header row can be removed.

 - Remove all \# characters from the file.

 - In the first column no separator value should come between the data-time, otherwise the date and time will be interpreted as two separate columns.

 - Moreover, the date should have a character inserted between YYYYMMDD, e.g., YYYY-MM-DD as above. Whatever character is used must be specified in OSCAR.

 - The file must be saved in CSV format.

Moreover, the hardware performance output from `Collectl` includes many columns that are not necessary for analyzing the data with `OSCAR`. The only measurements that need to be specified are the following columns: [CPU]Totl = Processor; [MEM]Used = Main memory - used kilobytes; [NET]RxKBTot = Network - Kilobytes received/s; [NET]TxKBTot = Network - Kilobytes sent/s; [DSK]ReadKBTot = Disk - Kilobytes read/s; and [DSK]WriteKBTot = Disk - kilobytes written/s. In the `OSCAR` interface, you can select NA for unused columns, e.g., "Auslastung Auslagerungsdatei".

Below is an example of the preprocessed results from `Collectl` measuring the performance data for Kate. The timestamp again increases in one-second increments.


| | | | | | | |
|-|-|-|-|-|-|-|
| Date_Time | [CPU]Totl% | [MEM]Used | [NET]RxKBTot | [NET]TxKBTot | [DSK]ReadKBTot | [DSK]WriteKBTot |
| 2022-08-27 16:47:10 | 1 | 7131968 | 0 | 0 | 0 | 0 |
| 2022-08-27 16:47:11 | 4 | 7131968 | 0 | 0 | 0 | 0 |
| 2022-08-27 16:47:12 | 1 | 7131968 | 0 | 0 | 0 | 0 |
| 2022-08-27 16:47:13 | 1 | 7131968 | 0 | 0 | 0 | 120 |
| 2022-08-27 16:47:14 | 1 | 7131968 | 0 | 0 | 0 | 0 |
| 2022-08-27 16:47:15 | 1 | 7131968 | 0 | 0 | 0 | 56 |
| 2022-08-27 16:47:16 | 1 | 7131968 | 0 | 0 | 0 | 48 |
| 2022-08-27 16:47:17 | 1 | 7131968 | 0 | 0 | 0 | 0 |
| 2022-08-27 16:47:18 | 1 | 7131968 | 0 | 0 | 0 | 0 |
| 2022-08-27 16:47:19 | 4 | 7131968 | 0 | 0 | 0 | 132 |

#### Uploading Data

Once the necessary files are ready, you can begin the analysis of the idle mode and standard usage scenario measurements using [OSCAR](https://oscar.umwelt-campus.de/). OSCAR will generate a summary report you can use for eco-certification or for your own purposes. In the OSCAR interface, note the following:

 - The interface language is currently German; see below for some translations.

 - The duration of the measurements in seconds must be specified.

 - [TODO: CHECK] Only the semicolon must be used as a separator.

 - The correct formatting of the time stamp must be specified for each of the uploaded files, e.g.,`%Y-%m-%d %H:%M:%OS`.

##### Step 1: Obtain Measurement Data

As stated at the landing page of the website (see below), the first step is obtaining measurement data (German: *Erfassung Messdaten*). 

![Oscar screenshot with the first step of obtaining measurement data (German: "Erfassung Messdaten").](images/sec3_oscar_1_landing.png)

##### Step 2: Upload Measurement Data

Once you have the baseline, idle mode, and usage scenario measurements, click on *(2) Upload Messdaten*, which in English means "upload measurement data".

Under *Messungen*, you upload either the idle mode or standard usage scenario measurements.  Under *Baselines* you upload the baseline measurements. For all data, the log file of actions taken (German: *Aktionen*), energy consumption (German: *Elektrische Leistung*), and hardware performance data (German: *Hardware-Auslastung*) are uploaded.

For *Art der Messung* ('Type of Measurement') in the lower right of the following screenshot, select *Leerlauf* ('Idle Mode') or *Nutzungsszenario* ('Usage Scenario') depending on which report you wish to generate (here *Nutzungsszenario* was selected). Note that the baseline measurements are always uploaded along with the idle mode or standard usage scenario measurements. It is important to also indicate the duration of the individual measurements in seconds (German: *Dauer der Einzelmessungen (s)*). See below for what a completed upload for the *Nutzungsszenario* with a duration of 217 seconds looks like:

![Oscar screenshot with the second step of uploading measurement data (German: "Upload Messdaten").](images/sec3_oscar_2_upload.png)

Once the data has been uploaded, you will need to tell OSCAR how to read the data. Let us start with the timestamp, since this is one aspect of the process which can cause problems if not done correctly. As an example, consider the Okular data:

 - For the log file of actions (German: *Aktionen*), the datetime is encoded as *YYYY-MM-DD HH:MM:SS*, e.g., "2022-10-04 12:32:43.656" in the data "okularActions.csv" for the baseline measurements (note the hyphen in the date; OSCAR will take care of the fractional seconds).
  
   In OSCAR this is specified so: "%Y-%m-%d %H:%M:%OS" (see screenshot below).

 - For the energy consumption data (German: *Elektrische Leistung*), the datetime is encoded as *DD.MM.YY, HH:MM:SS*, e.g.,  "04.10.22, 12:32:43" in the data "okular_baseline_eletrLeistung.csv" for the baseline measurements (note the period in the date and the comma seperating the date and time, as well as only having two digits for the year).
  
   In OSCAR this is specified so: "%d.%m.%y, %H:%M:%OS" (see screenshot below), in which the lowercase "%y" indicates only two digits for the year.

 - For the hardware performance data (German: *Hardware-Auslastung*), the datetime is encoded as *DD.MM.YYYY HH:MM:SS*, e.g.,  "04.10.2022 12:31:43" in the data "baseline_hardware_formatiert.csv" for the baseline measurements (note the period in the date and having four digits for the year).
  
   In OSCAR this is specified so: "%d.%m.%Y %H:%M:%OS" (see screenshot below), in which the uppercase "%Y" indicates four digits for the year.

![Oscar screenshot for uploading the measurement data where one specifies the format of the timestamp (German: "Formatierung Zeitstempel").](images/sec3_oscar_3_timestamp.png)

After the timestamps have been correctly specified, let's look at reading the format of the measurement data (German: *Formatierung Messdaten*) in OSCAR. First, we will do so for the log file of actions (German: *Aktionen*). Here you need to indicate for the uploaded CSV file the separator (German: *Trennzeichen*), the string delimiter (German: *Textqualifizierer*), the decimal separator (German: *Dezimaltrennzeichen*). For the Okular data, this is defined as follows in the following screenshot: semi-colon separator, double quotation string delimiter, and a period or full-stop decimal separator.

Additionally you will need to indicate whether the first line contains headings (German: *Erste Zeile enthält Überschriften*); if necessary, the number of lines to skip (German: *Anzahl zu überspringender Zeilen*); and the character encoding (German: *Zeichensatz (Encoding)*). For the Okular data, this is defined as follows in the following screenshot: first line contains headings is unchecked, 0 lines are skipped, and character encoding is utf-8. Since everything is specified correctly, a preview of the spreadsheet is shown in OSCAR.

![Oscar screenshot for the log file of actions (German: "Aktionen") where one specifies the format of the measurement data (German: "Formatierung Messdaten").](images/sec3_oscar_4_actions.png)

For the energy consumption measurements (German: *Elektrische Leistung*), the required input is the same as for the log file of actions, seen in the following screenshot. For the sake of concreteness, for the Okular data there is a semi-colon separator, double quotation string delimiter, and the character encoding is utf-8. However, now the decimal separator is a comma, that the first line contains headings is checked, and 1 line is skipped. You can confirm this is correct by inspecting the CSV file directly. Since everything is specified correctly, a preview of the spreadsheet is shown in OSCAR.

![Oscar screenshot for the energy consumption measurements (German: "Elektrische Leistung") where one specifies the format of the measurement data (German: "Formatierung Messdaten").](images/sec3_oscar_5_energy.png)

For the hardware performance data (German: *Hardware-Auslastung*), the required input is the same, but now with the additional requirement of specifying the columns. For the sake of concreteness, in the following there is a semi-colon separator, double quotation string delimiter, period or full stop decimal separator, that the first line contains headings is checked, 0 lines are skipped, and the character encoding is utf-8. For the columns (German: *Spalten*) specification, the following are defined:

 - *Zeitstempel*: Datetime (i.e., 'Date.Time')
 - *CPU-Auslastung*: CPU (i.e., 'X.CPU.Totl')
 - *RAM-Auslastung*: RAM (i.e., 'X.MEM.Used')
 - *Über Netzwerk gesendet*: Network sent (i.e., 'X.NET.TxKBTot')
 - *Über Netzwerk empfangen*: Network received (i.e., 'X.NET.RxKBTot')
 - *Von Festplatte gelesen*: Disk read (i.e., 'X.DSK.ReadKBTot')
 - *Auf Festplatte geschrieben*: Disk written (i.e., 'X.DSK.WriteKBTot')
 - *Auslastung Auslagerungsdatei*: Swap (here, 'N/A')

![Oscar screenshot for the energy consumption measurements (German: "Hardware-Auslastung") where one specifies the format of the measurement data (German: "Formatierung Messdaten").](images/sec3_oscar_6_hw.png)

#### Translations

Here is an overview of some of the terminology in German and their English translations:

 - *Messungen*: Measurements (e.g., Idle Mode or SUS)
 - *Aktionen*: Actions (i.e., log file of actions taken)
 - *Elektrische Leistung*: 'Electrical power' (i.e., energy consumption measurements)
 - *Hardware-Auslastung*: 'Hardware load' (i.e., hardware performance measurements)
 - *Dauer der Einzelmessungen (s)*: 'Duration of the individual measurements (s)' (i.e., specify how long each iteration was in seconds)
 - *Art der Messung*: 'Type of measurement'
     - *Leerlauf*: 'Idle' (i.e., Idle mode)
     - *Nutzungsszenario*: 'Usage scenario' (i.e., SUS)

 - *Formatierung Messdaten*: 'Formatting measurement data'
 - *Formatierung Zeitstempel*: 'Formatting timestamp'

 - *Trennzeichen* 'Separator'
 - *Textqualifizierer*: 'String delimiter'
 - *Dezimaltrennzeichen*: 'Decimal separator'
 - *Erste Zeile enthält Überschriften*: 'First line contains headings'
 - *Anzahl zu überspringender Zeilen*: 'Number of lines to skip'
 - *Zeichensatz (Encoding)*: 'Character set (encoding)'
 
 - *Spalten*: Columns
     - *Zeitstempel*: 'Datetime'
     - *CPU-Auslastung*: 'CPU utilization'
     - *RAM-Auslastung*: 'RAM utilization'
     - *Über Netzwerk gesendet*: 'Sent via network'
     - *Über Netzwerk empfangen*: 'Received via network'
     - *Von Festplatte gelesen*: 'Read from disk'
     - *Auf Festplatte geschrieben*: 'Written to disk'
     - *Auslastung Auslagerungsdatei*: 'Swap file utlization'

#### Downloading Reports

After completing the above, the report can be generated and downloaded, resulting in two documents: a report for the baseline and idle mode, and a report for the baseline and standard usage scenario.

![TODO: ADD CAPTION](images/sec3_oscar_7_report.png)

### Submitting Documentation For Blue Angel

For Blue Angel eco-certification, it is necessary to complete several documents with the above information as well as the energy consumption reports. The information that needs to be included is as follows:

 - Details about the software (name, version, etc) and measurement process (when and where measurements were made, etc.)

 - Technical details about the power meter (instrument, sampling frequency, length of scenario, sample size)

 - Technical details about the reference system (year, model, processor, cores, etc.)

 - Software stack used for measurements (`xdotool`, `Collectl`, etc.)

 - Minimum system requirements (processor architecture, local working memory, etc.)

 - Energy consumption measurements are found in the OSCAR reports or equivalent.

 - Hardware utilization measurements, which includes the following (for IDLE use Idle Mode measurements, and for SUS use SUS measurements):
   - *Full Load*: "For processing power, the full load is 100%, for working memory the sum of the installed RAM capacities, for network bandwidth the maximum transmission speed, etc." (Blue Angel award criteria: p. 23)
   - *Base Load*: Average load for the reference system in Baseline measurements
   - *Idle/SUS Load*: Average load for the reference system for IDLE/SUS measurements
   
     From the above measurements, the following calculations are made for hardware utilization (for IDLE use Idle Mode measurements, and for SUS use SUS measurements):
     - *Net Load*: IDLE/SUS Load - Base Load
     - *Allocation Factor*: Net Load/(Full Load - Base Load)
     - *Effective Load*: Net Load + Allocation Factor * Base Load
     - *Hardware Utilization* (SUS only): Effective Load * Time (seconds)

### More References

The measurement process is also described in the following documents:

 - [Resource and Energy-Efficient Software Products: Basic Award Criteria (Edition January 2020, Version 1)](https://www.blauer-engel.de/en/productworld/resources-and-energy-efficient-software-products)

 - Mai, Franziska (2021) *Vergleichende Analyse und Bewertung von Betriebssystemen hinsichtlich ihrer Energieeffizienz* (German only),

 - Seiwert & Zaczyk (2021) [*Projektbericht: Ressourceneffiziente Softwaresysteme am Beispiel von KDE-Software*](https://invent.kde.org/teams/eco/feep/-/blob/master/measurements/abschlussbericht-kmail-krita.pdf) (German only),

 - [OSCAR Manual](https://gitlab.umwelt-campus.de/y.becker/oscar-public/blob/master/OSCAR/static/OSCAR_Gesamt.pdf) (German only), and

 - Section 4.1 of Kern et al. (2018), *[Sustainable software products--Towards assessment criteria for resource and energy efficiency](https://doi.org/10.1016/j.future.2018.02.044)*.

### <a name="sec:gosundPM"></a> Alternative: Gosund SP111 Setup

Want to get started with the process, but short on cash or gear? Want to give the process a try without setting up a dedicated lab? Try this hack converting an inexpensive power plug to a power meter, courtesy of Volker Krause, who also documented the process in detail. You can read more at the following blog posts from Volker's blog:

 - ["Cheap Electric Power Measurement"](https://web.archive.org/web/20221002125057/https://volkerkrause.eu/2020/10/17/kde-cheap-power-measurement-tools.html)

 - ["KDE Eco Sprint July 2022"](https://web.archive.org/web/20220819231538/https://www.volkerkrause.eu/2022/07/23/kde-eco-sprint-july-2022.html)

Below is a guide to setting up a [Gosund SP111](https://templates.blakadder.com/gosund_SP111_v2.html) power plug already [flashed](https://github.com/tasmota/docs/blob/development/docs/devices/BlitzWolf-SHP6.md) with [Tasmota firmware](https://tasmota.github.io/docs) in 10 or fewer steps.

Although the data from this inexpensive power meter will likely not be accepted by the Blue Angel for eco-certification, it is nonetheless possible to obtain preliminary data using this tool.

 - (0) *Prerequisite*
 
    It is necessary to have the power plug already flashed with a sufficiently new Tasmota version.

 - (1) *Firmware Reset*
 
    If the device had previously been connected to another Wifi it might need a full reset before being able to connect to a new one.

    If the device did open a WiFi access point named "tasmota-XXXXX" this is not needed, continue directly to (2).

    Press the button for 40 seconds.

    The device will restart and you should be able to continue at (2).

 - (2) *WiFi Setup*

    The device opens a WiFi access point named "tasmota-XXXXX"&mdash;connect to that.

    Open http://192.168.4.1 in a browser.

    The device asks you for the WiFi name and password to connect to after entering those. The device will reconnect to that WiFi and disable its access point.

    While doing that it should show you its new address in the browser &mdash; make a note of that.

    In case that did not happen, check your WiFi router for the address of the device.

 - (3) *Tasmota Setup*

    Open the address from step (2) in a browser.

    You should see the Tasmota web UI (a big "ON/OFF" text and a bunch of blue and one red button).

    Click "Configuration".

    Click "Configure Other".

    Copy

            {"NAME":"Gosund SP111 2","GPIO":
            [56,0,57,0,132,134,0,0,131,17,0,21,0],"FLAG":0,
            "BASE":18}

    into the template input field.

    Tick the "Activate" checkbox.

    Click "Save".

    The device will restart; connect to it again.

    The UI should now also contain text fields showing electrical properties, and the "Toggle" button should now actually work.

 - (4) *Calibration*

    Open the address from step (2) in a browser.

    Connect a purely resistive load with a known wattage, such as a conventional light bulb (not a LED or energy-saving bulb).

    Switch on power by clicking "Toggle" if needed.

    Verify that the "Power Factor" value is shown as 1 (or very close to 1); if it is lower the current load is not suited for calibration.

    Click "Console".

    Enter the following commands one at a time and press enter:

          AmpRes 3  
          VoltRes 3  
          EnergyRes 3  
          WattRes 3  
          FreqRes 3  
          SetOption21 1﻿  
          VoltageSet 230

    Enter the command PowerSet XXX with XXX replaced by the wattage specified for the test load (e.g., "40" for a 40W light bulb).

    Click "Main Menu".

    The main page now should show correct power readings with several decimals precision.

 - (5) *MQTT Broker Setup*

    The only known way for high-frequency automatic readouts so far is polling over MQTT. This is not ideal and needs additional setup, unfortunately.

    If you happen to have a MQTT Broker around already, skip to step (6); otherwise, you need to set one up. The below scenario assumes Mosquitto is packaged for your distribution (and therefore does not configure any security), so only do this in your own trusted network and switch it off when not needed.

   - install the `mosquitto` package
   - add a file `/etc/mosquitto/conf.d/listen.conf` with the following content:

          listener 1883  
          allow_anonymous true

   - start Mosquitto using `systemctl start mosquitto.service`

 - (6) *MQTT Tasmota Setup*

    Connect to the Tasmota device using a web browser, and open the MQTT configuration page via Configuration > Configure MQTT.
    
    Enter the IP address of the MQTT broker into the "Host" field.

    Note down the value shown right of the "Topic" label in parenthesis (typically something like "tasmota_xxxxxx"). This will be needed later on to address the device via MQTT. You can also change the default value to something easier to remember, but this has to be unique if you have multiple devices.

    Click "Save".

    The device will restart and once it is back you should see output in its Console prefixed with "MQT".

 - (7) *Verifying MQTT Communication*

    This assumes you have the Mosquitto client tools installed, which are usually available as distribution packages.

    You need two terminals to verify MQTT communication works as intended.

        - In terminal 1, run `mosquitto_sub -t 'stat/<topic>/STATUS10'`
        - In terminal 2, run `mosquitto_pub -t 'cmnd/<topic>/STATUS' -m '10'`

    Replace `<topic>` with the value noted down in step (6).

    Everytime you run the second command, you should see a set of values printed in the first terminal.

 - (8) *Continuous Power Measurements*

    See [these scripts](https://volkerkrause.eu/2020/10/17/kde-cheap-power-measurement-tools.html).

 - (9) *Switching WiFi Networks*

    For security reasons, once connected to a WiFi network, Tasmota will not let you get back to step (2) by default without hard resetting the device (40-second button press). That, however, also removes all settings and the calibration. If you need to move to a different network, there are less drastic options available, but these changes can only be made inside the network you originally connected to:

    Under Configuration > Configure WiFi, you can add details for a second WiFi access point. Those will be tried alternatingly with the first configuration by default. This does not compromise security, but requires you to know the details for the network you want to connect to.

    You can configure Tasmota to open an access point as in step (2) by default for a minute or so after boot, and then try to connect to the known configurations. This makes booting slower in known networks, and opens the potential for hijacking the device, but it can be convenient when switching to unknown networks. This mode can be enabled in the Console by the command `WifiConfig 2`, and disabled by the command `WifiConfig 4`.

    For Tasmota version 11 the 40-second button press reset can leave the device in a non-booting state, whereas resetting from the Console using `Reset 1` doesn't have that problem, but has to be done before disconnecting from the known WiFi as well.

 - (10) *Recovering Non-Booting Devices*
 
    First and foremost: **DO NOT CONNECT THE DEVICE TO MAIN POWER**! That would be life-threatening. The entire flashing process is solely powered from 3.3V supplied by the serial adapter. Do not do any of this without having read this [getting started guide](https://tasmota.github.io/docs/Getting-Started/).

    With Tasmota 11, you can end up in a non-booting state by merely resetting the device using the 40-second button press. This does not permanently damage the device, and it can be fixed with reflashing via a serial adapter.

    The basic process is described in the above [guide](https://tasmota.github.io/docs/Getting-Started/). The PCB layout of the Gosund SP 111 can be seen [here](https://templates.blakadder.com/gosund_SP111_v1_1).

    In order for this to work, you need to connect GPIO0 (second pin on bottom left in the above image) to GND **before** powering up (i.e., before connecting with USB). The device LEDs (red and blue) are a useful indicator of whether you ended up in the right boot mode: the red LED should be on, and not flashing quickly, and the blue and red LED should not be on together. Once in that state, the connection can be removed (e.g., if you just hold a jumper cable to the pin) and it will remain in the right mode until a reboot.

    Again: **DO NOT CONNECT THE DEVICE TO MAIN POWER** as this is life-threatening; see above.

### Other Initiatives Working On Sustainable Software Tooling

There are many initiatives working on digital sustainability by providing the tooling necessary for measuring the energy consumption of software. We would like to mention four in particular who have been working together with the KDE Eco initiative:

 - [The Green Software Engineering work group](https://www.umwelt-campus.de/en/green-software-engineering) at the [Environmental Campus Birkenfeld](https://www.umwelt-campus.de/en/)
 
   Since 2008 the Green Software Engineering work group have been working on research projects with a focus on sustainable software. Their team developed tools such as `OSCAR` and have measured various KDE applications, including Okular.

 - [Green Coding Berlin](https://www.green-coding.org/)
 
   Green Coding Berlin is focused on research into the energy consumption of software and its infrastructure, creating open source measurement tools, and building a community and ecosystem around green software. 

 - The [SoftAWERE](https://sdialliance.org/steering-groups/softawere) project from the [Sustainable Digital Infrastructure Alliance](https://sdialliance.org/)
 
   The SoftAWERE steering group oversees and sets the direction for the development of tools and labels for energy-efficient software applications.

 - [Green Web Foundation](https://www.thegreenwebfoundation.org/)
 
   The Green Web Foundation tracks and accelerates the transition to a fossil-free internet.

## (B) Hardware Operating Life

Many FOSS applications run on hardware much older than 5 years. In fact, members of the KDE community have noted that KDE's desktop environment `Plasma` runs on hardware from even 2005!

This category is relatively easily to fulfill for the Blue Angel application. To demonstrate compliance for Blue Angel certification, one needs the following information:

 - *Reference System Year* &mdash; e.g., 2015

 - *Model* &mdash; e.g., Fujitsu Esprimo 920

 - *Processor* &mdash; e.g., Intel Core i5-4570

 - *Cores* &mdash; e.g., 4

 - *Clock Speed* &mdash; e.g., 3,6 GHz

 - *RAM* &mdash; e.g., 4 GB

 - *Hard Disk (SSD/HDD)* &mdash; e.g., 500 GB

 - *Graphics Card* &mdash; e.g., Intel Ivybridge Desktop

 - *Network* &mdash; e.g., Realtek Ethernet

 - *Cache* &mdash; e.g., 6144 KB

 - *Mainboard* &mdash; e.g., Fujitsu D3171-A1

 - *Operating System* &mdash; e.g., Ubuntu 18.04

For Blue Angel eco-certification, the above data needs to be added to 2 documents called "Annex 1" and "Annex 2". You can find examples for Okular at the following links:

 - Annex 1: https://invent.kde.org/teams/eco/blue-angel-application/-/blob/master/applications/okular/de-uz-215-eng-annex-1-okular.docx
 - Annex 2: https://invent.kde.org/teams/eco/blue-angel-application/-/blob/master/applications/okular/de-uz-215-eng-annex-2-okular.xlsx

## (C) User Autonomy

As discussed in Part II, the Blue Angel user autonomy criteria cover eight general areas:

  1. Data Formats

  2. Transparency

  3. Continuity Of Support

  4. Uninstallability

  5. Offline Capability

  6. Modularity

  7. Freedom From Advertising

  8. Documentation

Many FOSS projects may take for granted that Free Software respects user autonomy, and in some cases information from the above list is missing from websites, manuals, wikis, etc&mdash;things like support for open standards in data formats, uninstallability, or continuity of support.

Documenting this information is important, both for fulfilling the Blue Angel award criteria and for giving users information about long-term sustainable use of their software and hardware. KDE and FOSS communities know Free Software respects its users&mdash;now, let's make sure everyone else knows it too!

This is not an exhaustive presentation for each of the above categories of the Blue Angel criteria. Rather, this guide focuses on aspects of the criteria which KDE/FOSS projects can easily document and provide (which is already most of the work). For the full criteria, see Section 3.1.3 in the [award criteria](https://produktinfo.blauer-engel.de/uploads/criteriafile/en/DE-UZ%20215-202001-en-Criteria-2020-02-13.pdf).

### 2.1 Data Formats

The main information to include in documentation:

 - Which (open) data formats are supported&mdash;with links to specifications, e.g., [PDF](https://www.iso.org/standard/51502.html)?

 - Also of interest: Are there examples of other software products that process these data formats?

For an example of the online documentation of supported data formats for Okular, see https://okular.kde.org/formats/.

For an example of documentation in a Blue Angel application, see https://invent.kde.org/teams/eco/blue-angel-application/-/blob/master/applications/okular/de-uz-215-eng-annex-4-okular.md.


### 2.2 Transparency Of The Software Product

When missing, provide links to documentation of the API, source code, and the license.

For the sake of example, for KMail:

 - KDE PIM API documentation: https://api.kde.org/kdepim/index.html.

 - Source code: https://invent.kde.org/pim/kmail.

 - License: https://invent.kde.org/pim/kmail/-/blob/master/LICENSES.

For an example of documentation in a Blue Angel application, see: https://invent.kde.org/teams/eco/blue-angel-application/-/blob/master/applications/kmail/de-uz-215-eng-annex-5-kmail.md.


### 2.3 Continuity Of Support

Examples of details about continuity of support to add to documentation:

 - Information about how long the software has been supported for (with links to release announcements).

 - Release schedule and details (e.g., who maintains the software).

 - Statement that updates are free of charge.

 - Declaration on how the free and open source software license enables continuous support indefinitely.

 - Information about whether, and how, functional and security updates may be installed separately.

An example of Okular's continuity of support documentation for Blue Angel certification can be found in Section 3.1.3.3 here: https://invent.kde.org/teams/eco/blue-angel-application/-/blob/master/applications/okular/de-uz-215-eng-annex-6-okular.md.

### 2.4 Uninstallability

How can one completely uninstall the software? Relevant details may include:

 - Uninstallation depends on how the software was installed (source code or binary).

 - Examples of uninstallation instructions (source code or package managers, with relevant links to documentation).

 - Information about whether user-generated data is also removed when uninstalling a program.

An example of Okular's uninstallability documentation for Blue Angel certification can be found in Section 3.1.3.4 here: https://invent.kde.org/teams/eco/blue-angel-application/-/blob/master/applications/okular/de-uz-215-eng-annex-6-okular.md.

### 2.5 Offline Capability

This could be one of the easiest areas for most FOSS projects to document (alongside freedom from advertising; see below): Does the software require external connections such as a license server in order to run? If not, and no network connection is needed and the software is capable offline, this should be documented.

An example of Okular's offline capability documentation for Blue Angel certification can be found in Section 3.1.3.5 here: https://invent.kde.org/teams/eco/blue-angel-application/-/blob/master/applications/okular/de-uz-215-eng-annex-6-okular.md.

### 2.6 Modularity

Information to document includes:

 - What aspects of the software are modular and can be deactivated during installation?

 - Can the software manuals or translations be installed separately?

 - Are any modules unrelated to the core functionality of the software included with installation, such as tracking modules or cloud integration? If not, document it!

An example of Okular's modularity documentation for Blue Angel certification can be found in Section 3.1.3.6 here: https://invent.kde.org/teams/eco/blue-angel-application/-/blob/master/applications/okular/de-uz-215-eng-annex-6-okular.md.

### 2.7 Freedom From Advertising

If the software does not display advertising, make this explicit in manuals and wikis and declare it in the Blue Angel application document.

### 2.8 Documentation

This includes the following:

 - General process for installing/uninstalling the software? (This may include generic instructions or tutorials for a specific desktop environment or package manager.)

 - Data import/export process?

 - What can users do to reduce the use of resources (e.g., configuration options for improving performance)?

 - Does the software have any resource-intensive functionality not necessary for the core functionality? If not, great. Let's tell the users!

 - Licensing terms related to further development of the software products, with links to source code and license?

 - Who supports the development of the software?

 - Does the software collect any personal data? Is is compliant with existing data protection laws? If yes, document it!

 - What is the privacy policy? Is there telemetry, and if yes, how does the software handle data security, data collection, and data transmission? Also, are there ads or tracking embedded in the software? If not, excellent&mdash;now make sure to spread the word!

An example of Okular's product documentation for Blue Angel certification can be found in Section 3.1.3.8 here: https://invent.kde.org/teams/eco/blue-angel-application/-/blob/master/applications/okular/de-uz-215-eng-annex-6-okular.md.

#### Examples

Below are examples of Blue Angel documentation for Okular.

 - Annex 4: Data Formats (Section 2.1)
   - https://invent.kde.org/teams/eco/blue-angel-application/-/blob/master/applications/okular/de-uz-215-eng-annex-4-okular.md

 - Annex 5: Open Standards (Transparency Section 2.2)
   - https://invent.kde.org/teams/eco/blue-angel-application/-/blob/master/applications/okular/de-uz-215-eng-annex-5-okular.md

 - Annex 6: Product Information (Sections 2.3–2.8)
   - https://invent.kde.org/teams/eco/blue-angel-application/-/blob/master/applications/okular/de-uz-215-eng-annex-6-okular.md

 - See also Annex 1
   - https://invent.kde.org/teams/eco/blue-angel-application/-/blob/master/applications/okular/de-uz-215-eng-annex-1-okular.docx
   

## Submitting Your Application For The Blue Angel

Once you have all of the above documentation prepared, you need to submit it for review to RAL gGmbH (if you recall, RAL is the authorized body that assesses compliance with the award criteria). The portal for submitting Blue Angel applications is here:

 - https://portal.ral-umwelt.de/.

If you need help with the online interface, RAL provides [documentation](https://portal.ral-umwelt.de/RALUmwelt/Anleitung/1).

## Benefits Of Blue Angel

Steffi Lemke, Federal Minister for the Environment, Nature Conservation, Nuclear Safety and Consumer Protection (German: *Bundesministerium für Umwelt, Naturschutz, nukleare Sicherheit und Verbraucherschutz*, or BMUV), has said [about the reputation of the Blue Angel](https://www.blauer-engel.de/en/blue-angel/our-label-environment):

> An increasing number of people focus on durability and environmental friendliness when purchasing products. This is precisely what the Blue Angel stands for. The ecolabel has been a guarantee of high standards for the protection for our environment and health for 40 years in an independent and credible way.

Indeed, in their 40<sup>th</sup> anniversary info-booklet ["Blue Angel &ndash; 40 years. Good for me. Good for the environment"](https://www.umweltbundesamt.de/sites/default/files/medien/1410/publikationen/uba_40jahreblauerengel_publikation_en_web.pdf), the German Environment Agency (UBA) explored the history, present, and future of the ecolabel. In the booklet they identify some of the general criteria they consider when eco-certifying a product, such as:

 - reduced emissions of harmful substances in the ground, air, water and indoors;

 - sustainable production of resources;
 
 - longevity, ability to repair and recycle the product; and

 - efficient use, e.g. products which save energy.

As you reach the end of this handbook, we hope it is clear how eco-certification of desktop software promotes the above environmental benefits, among others.

Environmental labels can be an instrument to move markets in the direction of sustainable products. The Blue Angel website states, "The aim of the environmental label is to provide private customers, large institutional consumers and public institutions with reliable guidance for environmentally conscious purchasing."

So what does the market say?
 
A survey from the above info-booklet found that 92% of Germans recognize the ecolabel, and for 37% the label influences their purchasing choices. The label is recognizable outside of Germany, too! In conversations with representatives at the German Environment Agency we were informed that up to 15% of Blue Angel recipients are outside of Germany. One reason for this is that unlike some other ecolabels, the Blue Angel puts no requirements on where a product can be marketed. Moreover, the Blue Angel seal is considered a mark of high quality internationally, and the award criteria are viewed as an indicator of direction of the EU market&mdash;and even used as a guideline for optimizing products.

Receiving the Blue Angel seal can raise your product's profile not only among individuals but also large organizations. [Green Public Procurement](https://en.wikipedia.org/wiki/Sustainable_procurement) (GPP) initiatives, which "seek to promote the public procurement of goods, services, and works with a reduced environmental impact throughout their life-cycle" ([European Commission](https://ec.europa.eu/environment/gpp/faq_en.htm)), influence purchasing choices both in the public and [private sector](https://en.wikipedia.org/wiki/Sustainable_procurement#Private_sector). Eco-certifying your software product with the Blue Angel demonstrates a commitment to long-term digital sustainability, and it gives your product visibility both in Germany and abroad.