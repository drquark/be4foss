
# About The Authors

KDE Eco tooling and documentation are provided by community members who have volunteered to contribute to this project for the benefit of all. Primary contributors include (listed in alphabetical order by first name): Arne Tarara, Cornelius Schumacher, David Hurka, Emmanuel Charruau, Karanjot Singh, Nicolas Fella, and Volker Krause. Your contributions make this handbook possible.

The text of this version of the handbook was written and/or compiled from the above documentation by Joseph P. De Veaugh-Geiss. Olea Morris helped edit the text. Lana Lutz and Arwin Neil Baichoo made the book and website design as well as the images therein beautiful. Paul Brown made significant improvements to the Okular blog post which was included in modified form in [Part II: First Eco-Certified Computer Program: KDE's Popular Document Reader Okular](#sec:sec:okular). Wikipedia was a source of several useful texts which were included here in modified form: thank you to the community of Wikipedia writers and editors. See the end of each section for additional information about sources.

# Acknowledgments

Thank you to the many contributors to the KDE Eco initiative in general (listed in alphabetical order by first name): Achim Guldner, Adriaan de Groot, Aleix Pol, Alexander Semke, André Pönitz, Björn Balazs, Carl Schwan, Chris Adams, Christopher Stumpf, Fabian, Franziska Mai, Harald Sitter, Johnny Jazeix, Jonathan Esk-Riddell, Kira Obergöker, Lydia Pintscher, Max Schulze, Phu Nguyen, Sami Shalayel, Stefan Naumann, Sven Köhler, and Tobias Fella. Your contributions are greatly appreciated.

People who are interested in contributing to KDE Eco are encouraged to express interest at the [mailing list](https://mail.kde.org/cgi-bin/mailman/listinfo/energy-efficiency) or [Matrix room](https://webchat.kde.org/#/room/#energy-efficiency:kde.org). Contributors are also invited to join one of the KDE Eco sprints and in-person or online meetups. Learn more at out website: <https://eco.kde.org/get-involved/>

There were additionally many informative conversations that happened at one of these conferences or workshops: Akademy 2022, Linux App Summit 2022, rC3: NOWHERE 2021, SFSCon 2021/2022, Grazer Linuxtage 2022, Qt World Summit 2022, QtDevCon 2022, Fedora Nest 2022, Green Coding Berlin meetups, Sustainable Digital Infrastructure Alliance hackathon, EnviroInfo 2022, and Bits & Bäume 2022. Thank you!

# License

Unless indicated otherwise, the contents of this handbook are released under the <a href="https://spdx.org/licenses/CC-BY-SA-4.0.html">Creative Commons Attribution-Share-Alike License 4.0 (CC-BY-SA 4.0)</a>. For more information about documentation licensing at KDE, see [KDE's licensing policy](https://community.kde.org/Policies/Licensing_Policy).