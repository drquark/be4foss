The handbook is broken up until smaller sections in the directory `sections/`.

To build full handbook from the individual sections:

$ `cat sections/0*.md sections/1*.md sections/2*.md sections/3*.md sections/4*.md > handbook.md`

It it important to indicate the file extension `*.md` in the event there are automatically-generated backup files (e.g., from KDE's `ghostwriter`) in the directory.

Alternatively one can build parts of the handbook by indicating which sections to include. For instance, the following command will only include the sections in part one.

$ `cat sections/1*.md > handbook_partI.md`
