# Minutes for the community meetup on 2022-09-14.

## Details

* Start: 19:00 CEST; End: 20:00 CEST

* 15 in person participants + 2 online participants + 2 BE4FOSS organizers [names removed to protect participant privacy]

* *Topic*: Overview of the whys and whats of the KDE Eco project.

* *Where*: A hybrid in-person/online event at the Green Coding Berlin meetup in Berlin.

## Notes

* Intro to presenter
    * Presentation: https://invent.kde.org/teams/eco/be4foss/-/blob/master/conferences-workshops/presentations/2022-09-14_community-meetup_green-coding-berlin_presentation.pdf
* Questions for the in-person group:
    * How many of you feel like you have control over your digital life?
    * How many feel that your digital life is transparent?
    * How many know what free and open software is?
* KDE is nonprofit and one of the oldest communities of free software.
* KDE’s vision:"A world in where everyone has control over their digital life and enjoys freedom and privacy." 
* Connecting that vision to sustainability
* You can collaborate by joining in various ways (please see website)
* Background: ICT CO2 emission estimated between 1.8% - 2.8%
* Graph of distribution of energy consumption
* Today: energy efficiency, energy conservation, energy sources
* Graph: Comparison of different word processors, issue of one word processor consuming 4X the energy
    * Thinking about it at scale: 1 CPU second reduction can quickly have huge effects at scale
* Issue of software-driven hardware obsolescence and wasted energy in production and transportation of new devices, unnecessary e-waste
* Energy sources can be used for update timing to maximize renewable sources
* FOSS is socially-oriented, empowering users and communities, empowered to find new and creative ways to improve sustainability for this and future generations
* Green Coding Berlin has a tool to measure energy usage. (please see their website)
* Acting local in software also means acting global! Software is international.
* KDE has a label “efficiency” for merge requests since Nov 21, 130+ since then
* Current FSFE campaign to extend right to repair to include software and to upcycle unsupported smartphones
* Blauer Engel award criteria: transparency, user autonomy are fundamental to sustainability
* Motivation for KDE projects FEEP and BE4FOSS
* ABCs of award criteria: A) potential hardware operating life B) user autonomy (uninstallabilita/modularity, transparency, continuity of support, offline capability/freedom from advertising) C) resource & energy efficiency (hardware performance/energy consumption)
* 3 steps: measure, analyze, certify
* There is a recommended lab setup from BE
    * A lab was setup at KDAB Berlin in a KDE eco sprint that can be used by FOSS projects
* When measuring: 3 scenarios to measure (baseline, idle mode, standard usage scenario)
* Data analysis: OSCAR tool developed by Umwelt-Campus Birkenfeld, upload data and get report
* Okular is the first eco certified application by BE
* Local communities for sustainable digitization: 
    * Green Coding Berlin
    * SoftAWARE from SDIA
    * Green Web Foundation
* Q(uestions)/A(nswers)/C(omments):
    * Q: Have you applied for BE for Kmail yet? A: No, not yet
    * Q: How long did it take to get the certification for Okular? A: About 3 months
    * Q: Has there been a comparison of FOSS and non-FOSS operating systems? A: Some research at Umweltcampus but no official or public data.
    * Q: What do you think is the best change you can make? (Audience response)