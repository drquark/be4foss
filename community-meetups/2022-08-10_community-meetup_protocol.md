# Minutes for the call with the community on 2022-08-10.

## Details

* Start: 19:00 CEST; End: 20:00 CEST

* 4 Participants + 1 BE4FOSS organizer [names removed to protect participant privacy]

* *Topic*: Emmanuel and Karanjot presented KdeEcoTest / KdeEcoTestCreator, a Python-based tool using xdotool to emulate user behavior in Standard Usage Scenario scripts.

   [https://invent.kde.org/teams/eco/feep/-/tree/master/tools/KdeEcoTest](https://invent.kde.org/teams/eco/feep/-/tree/master/tools/KdeEcoTest)

* *Details*: Over the past several months, Emmanuel has been working on the development of KDE Test Creator / KDE Test, which is written in Python and uses xdotool to trigger actions in the software being tested. Karanjot Singh is also contributing to the project. A benefit of the KDE Test tools is that scripts are transferrable across different systems, while also being easy enough to use to make scripting quick. Emmnauel presented the tool to get feedback from the community and motivate those wishing to support development of the tool.

* From the GitHub repository:
> KdeTestCreator and KdeTest are tools which together aim at reproducing actions on a computer.
> They have been created in order to produce energy consumption measurements. Simulating human usage of a computer through a long period, they help to measure what the applications energy consomptions are and spot where they can be improved.

## Notes

* Round of introductions from participants
* Intro to the presenter and topic.
    * Presentation: 2022-08-10_community-meetup_presentation_KdeEcoTest.pdf
* Presentation overview: Other emulation tools, `KdeEcoTest`: what has been done thus far, demo, Q&A
* `Actiona`: mouse-click based, editor interface, not easy to reuse scripts, especially due to screen resolution issue; not programmer friendly, requires doing the same click-and-point actions over and over, not easily editable
* `xdotool`: CLI interface, works on X11, can use in scripts with bash, no longer have the screen resolution issue
* `Python` + `xdotool` make up the `KdeEcoTest` and `KdeEcoTestCreator` tools, major benefits are the scripts are easy to prepare and edit and can be transferred to other systems
* Demo: running KdeEcoTest with `GCompris` to show user behavior emulation.
* Q: The coordinates for clicking are relative to the window, can the window be any size?
* A: Screen size is set at the beginning of the scripts under "SetWindowToOriginalSize". Also, how many repetitions of the script is also set at the beginning.
* `KdeEcoTestCreator` is the program to write the script:
    * can easily add commands for actions to be taken;
    * a comment about the action isautomatically included;
    * everything can be easily modified or added to manually;
    * also possible to copy and paste coordinates when the click location or textbox is the same, this saves a lot of time;
    * emulation of scrolling is now possible.
* The example shown would have taken 15 minutes in `Actiona`, but took just seconds in `KdeEcoTestCreator`
* Q&A/Comments
    * Q: I have worked with Selenium-style testing. What is the landscape of tools you are using? Does one have to use `xdotool` with `Actiona`? Or are they separate?
    * A: They are two different tools. `KdeEcoTest` just embeds `xdotool` commands within `Python`.
    * C: Did you see the mail to mailing list today? Regarding Selenium, there are also recent emulation experiments going on in that direction: https://mail.kde.org/pipermail/plasma-devel/2022-August/122680.html.
    * Q: Have you tried running the `Actiona` script from Okular? Porting it to `xdotool`?
    * A: No.
    * Q: Can `xdotool` or `KdeEcoTest` run in a container?
    * A: An extension of X11 protocol. if you have X working, it should as well.
    * C: `xdotool` does not work (well) with Wayland. The situation is complicated, as Wayland is designed not to allow certain behavior which can present a security issue.
    * Q: Is there currently a possibility to know if the action succeeded?
    * A: No, `xdotool` does not confirm the action. We are not doing unit testing or system testing. Always checking the energy consumption being higher or lower.
    * Q: But perhaps you have a new iteration of your software, and the coordinates have changed. Can you know if the action succeeded?
    * A: Normally with pixel clicks will choose activities that are not moving.
    * C: Squish ([https://www.froglogic.com/](https://www.froglogic.com/)). Qt interface which allows inserting code in the test, and can offer testing functionality. Think of a web inspector as a remote API. Specific to the toolkit. Support also for other toolkits, but not as generic. There is also the problem with dialogs, which do not appear in the same place. Squish can do this. It finds the button, you do not tell the tool where to click.
    * C: Alternatively, one could look at the ouput to see if expected actions were undertaken (e.g., documents produced by text editors).
    * Q: Have you tried `KdeEcoTest` with other KDE software? Other font sizes? Resolutions?
    * A: Not really. For `Kate` bash+xdotool script used keyboard shortcuts, this is very robust, shortcuts do not change.