
# Table of Contents

1.  [DAY OVERVIEW](#org9113998)
2.  [GETTING STARTED](#org9d58be1)
3.  [AFTERNOON ACTIVITIES](#org259e3ec)
4.  [WRAP UP](#org9f244ec)


<a id="org9113998"></a>

# DAY OVERVIEW


## Plan

-   The goals of the Sprint are two-fold:
    -   i. To begin measuring KDE/Free Software such as Kate and GCompris as well as other projects that want to prepare usage scenarios.
    -   ii. To work on standardizing the data and developing the tools to automate the workflow and analyze/visualize the results.
-   Schedule (outline):
    -   11:00-12:30 preparation, start first measurements
    -   12:30-13:30 lunch
    -   13:30-16:30 continue measurements, work on tooling, labplot
    -   16:30-17:00 break
    -   17:00-19:00 wrap up, plan next steps
-   Links:
    -   KdeEcoTest & GCompris SUS: <https://invent.kde.org/teams/eco/feep/-/tree/master/tools/KdeEcoTest>
    -   Updated KdeEcoTestCreator: <https://invent.kde.org/echarruau/feep/-/merge_requests/1>
    -   Kate SUS: <https://invent.kde.org/teams/eco/be4foss/-/tree/master/standard-usage-scenarios/kate>
    -   Karanjot's repo: <https://invent.kde.org/drquark/feep>


## Achieved

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-right" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-right">TIME</th>
<th scope="col" class="org-left">ACHIEVED</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-right">11:00-12:30</td>
<td class="org-left">- Introductions, planning, and preparation</td>
</tr>
</tbody>

<tbody>
<tr>
<td class="org-right">12:30-13:30</td>
<td class="org-left">- Lunch</td>
</tr>
</tbody>

<tbody>
<tr>
<td class="org-right">13:30-15:00</td>
<td class="org-left">- Discussion: Carbon-reduced updates</td>
</tr>


<tr>
<td class="org-right">&#xa0;</td>
<td class="org-left">- Lab: GCompris installation, SUS script tests, update</td>
</tr>


<tr>
<td class="org-right">&#xa0;</td>
<td class="org-left">Python script to read data via SNMP</td>
</tr>


<tr>
<td class="org-right">&#xa0;</td>
<td class="org-left">- NeoChat: SUS scripting (KdeEcoTester)</td>
</tr>
</tbody>

<tbody>
<tr>
<td class="org-right">15:00-15:30</td>
<td class="org-left">- Video call re LabPlot</td>
</tr>
</tbody>

<tbody>
<tr>
<td class="org-right">15:30-18:00</td>
<td class="org-left">- Discussion: PM output interpretation</td>
</tr>


<tr>
<td class="org-right">&#xa0;</td>
<td class="org-left">- Lab: Update SUS script for GCompris to be more verbose</td>
</tr>


<tr>
<td class="org-right">&#xa0;</td>
<td class="org-left">- LabPlot: bug squashing</td>
</tr>


<tr>
<td class="org-right">&#xa0;</td>
<td class="org-left">- NeiChat: SUS Scripting</td>
</tr>
</tbody>

<tbody>
<tr>
<td class="org-right">18:00-18:45</td>
<td class="org-left">- Wrap up, clean up, and lock up</td>
</tr>
</tbody>
</table>

-   Total of 6 in-person participants, 3 virtual participants [names removed to protect participant privacy]


<a id="org9d58be1"></a>

# GETTING STARTED


## 11:00-12:00

-   Warm up, chat
-   Discussion of community goals:
    -   Testing setup and optimizing it
    -   Measuring GCompris/Kate, collecting data
    -   Reproducing labplot usage from Alexander (see mp4 in energy efficiency room)
    -   Gosund power plug usage documentation / possible how-to blog post for Green Web Foundation
    -   Neochat SUS/measurements
-   Break off into two groups
    -   Group 1 working in the lab on measurements
    -   Group 2 working in meeting room on labplot and gosund power plug measurements


## 12:00-12:30

-   Introduction round for new attendees; planning/discussion continues
-   Q: Browsers, do we want to test them? A: Not now. No widely-used KDE browser, all are chromium-derivatives anyway
-   Discussion: clarifying focus of measurement lab


<a id="org259e3ec"></a>

# AFTERNOON ACTIVITIES


## 13:30-15:00

-   Discussion: Would it be possible for KDE to have carbon-reduced updates and other shiftable tasks
    -   Idea: use information from power grid to shift tasks to maximize renewable energy sources
    -   Something like this has already been implemented by Microsoft
    -   See grid-intensity golang library tool that exposes carbon intensity: <https://github.com/thegreenwebfoundation/grid-intensity-go>
    -   In KDE, [Discover][<https://userbase.kde.org/Discover>] ([code][<https://github.com/KDE/discover>]) is the app store / packager manager tool for KDE. It has an internal daemon that periodically checks for updates, and downloads them.
    -   Screenshot for Discover: ![img](https://volkerkrause.eu/~vkrause/kde-eco/Screenshot_20220716_133830.png))
    -   Discover is written in C++. One could quie easily have it call out to the grid-intensity golang library before starting a software update to check if it should go ahead with computationally expensive process. You could have a periodic go/no check every N hours as Discover has its retry logic already. The component in the discover codebase is called the notifier. The functions in question are \`UnattendedUpdates::triggerUpdate\`, \`UnattendedUpdates::checkNewState\`.
    -   Could be implemented relatively easily for KDE, but would be a much larger project to extend to wider GNU/Linux distributions
    -   Funding to implement this: Prototype fund? Other? Discussion to be continued.

-   Measurement lab: Getting started
    -   Install GCompris v. 2.4 on neon
    -   Testing scripts (Kate, GCompris), resolving some issues (upper/lowercase toggle in Kate)
    -   Debugging GCompris script with KdeEcoTest
    -   Update python script with Python SNMP library [PySNMP][<https://pysnmp.readthedocs.io/en/latest/>]

-   Neochat:
    -   Set up a matrix server ([Conduit][<https://conduit.rs/>], not federated) to produce reproducible characteristics
    -   SUS: create some rooms, users


## 15:00-15:30

-   LabPlot video call, round of introductions
-   General goal of measurement labs is to compare to past consumption and see changes: is it more efficient, are there regressions?
-   Issue of LabPlot crashing at startup; could be due to particular developer setup on endusers' computers (Qt debug build)
-   We need both live data visualization and recorded data for LabPlot:
    -   (i) to plot data live as it comes in, and
    -   (ii) since data is dynamic, some averaging or smoothing to better be able to see what is happening (in other words, spikes are hard to interpret)
-   See video: 2022-07-16<sub>sprint03</sub><sub>LabPlot</sub><sub>live</sub><sub>data</sub><sub>demo.mp4</sub>
-   Also: want to have live view to experiment and also to demonstrate how user behavior influences consumption
    -   One idea: a plasma widget to display energy consumption to users (current energy consumption of what you are doing)
    -   Monitoring applet example (see 2022-07-16<sub>sprint03</sub><sub>LabPlot</sub><sub>example</sub>-for-live-monitoring.png)
-   Issue of side channel attack vectors?
-   Feedback desired on how to make data look nice, but at a later stage
    -   Use of existing templates to make things look nice for people who do not make things look nice :)


## 15:30-18:00

-   Update KdeEcoTest Python script to include verbose output of actions in GCompris and other scripts (thanks to virtual assistance)
-   Power Meter SNMP scripts issues: there is a unexpected pause in measurement output at 5/10 second intervals; since the issue exists also for the [Python script from Umwelt Campus Birkenfeld][<https://gitlab.rlp.net/green-software-engineering/mobiles-messgerat>], the source of the problem may be in the [Gude power meter][<https://www.gude.info/en/power-distribution/switched-metered-pdu/expert-power-control-1202-series.html>]
-   Discussion related to new Python script to read SNMP data: at the moment can request a single value with a call, but would like to call a bunch of data all at once; may not be possible, not made for high frequency continuous monitoring.
    -   Wat should frequency be? How fast can the pulling be?
    -   What do the values actually mean (see [this post][] to efficiency mailing list)
-   [Merge Request][<https://invent.kde.org/-/snippets/2281>] to fix crash issue in LabPlot
-   Funding discussion re prototype funding or other sources: <https://annuel2.framapad.org/p/2022-07_kde-greenweb-prototype-fund-notes-9vd2?lang=en>


<a id="org9f244ec"></a>

# WRAP UP


## 18:00-18:45

-   Wrap up, clean up, lock up at KDAB Berlin


## 18:45-19:45

-   Concluding chat, planning for future meetups and conference presentations

