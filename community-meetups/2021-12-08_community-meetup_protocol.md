Minutes for the call with the community on 2021-12-08.

Start: 19:00
End: 20:00

5 Participants + 1 BE4FOSS organizer [names removed to protect participant privacy]

Notes:

* Intro to the meeting, agenda, etc.
    * Topic: How to do automation using xdotool and KXMGUI
    * See guest presenter's script in repository for examples and details:
        2021-12-08_presentation_usage_scenarios.md
    * See also Kate usage scenario: https://invent.kde.org/-/snippets/1937
* xdotool
    * Moving mouse using coordinates
    * Clicking or holding mouse button down
    * Typing text
    * Emulating natural user behavior with sleep command
    * In KDE can assign shortcuts: Menu > Settings > Configure Shortcuts, useful for automation
    * Q: Can shortcut configurations be transferred to another computer? A: Yes, this should be possible
* KXmlGui
    * Global registry allowing for shortcut configurations, command bar customization
    * Assigning id to actions, e.g. "File > New" is "file_new"
    * D-BUS API can trigger the action
    * Command line tool like qdbus to call actions; see also qdbusviewer
    * Note that some KDE applications have additional qdbus options, can also get a list of all actions in the registry (see Kate example in presentation script)
* Q&A/Comments
    * Q: Which log should be monitored for timestamps? A: No log, need to echo current system time before each action (i.e., write a custom logger)
    * Q: Is there a need to define standard timestamp logger? A: There is no standard logger
    * Q: How to monitor HW usage capacity? A: Collectl tool (collectl -s cdmn -i1 -P --sep 59 -f /var/log/performanceMeasures.csv)
    * Q: How big is the impact of that continuous monitoring on the energy consumption?
    * Q: Wake up times/deep sleep states influencing energy consumption?
    * Lab setup of presenter
        * Ubuntu server image
        * Xserver and windows manager, unsintalled unnecessary server stuff (as minimal as possible)
        * Network connection control via ssh
        * Power Meter: Hioki PW3335
        * Standardized configuration files reinstalled each run