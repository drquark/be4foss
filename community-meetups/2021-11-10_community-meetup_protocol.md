Minutes for the call with the community on 2021-11-10.

Start: 19:00 CET
End: 20:00 CET

15 Participants + 2 BE4FOSS organizers [all names removed to protect participant privacy]

Plan:
* 10-15 minute presentation: Standard Usage Scenarios (SUS)
* 20-25 minute discussion: see below
* Two breakout rooms: Brainstorm usage scenarios for Kate and GCompris (see below)
* Main room: discuss emulation tools for implementing scenarios. Goal: Decide which we should use for measure-athon! (e.g., XMLGUI, Actiona, xdotool, see summary at https://invent.kde.org/cschumac/feep/-/blob/master/tools/presentation_Visual_Workflow_Automation_Tools/Visual_Workflow_Automation_Tools.pdf)
* 5-15 minute wrap-up: Questions re Sprint 1 (11 December), Planning for Sprint 2 the measure-athon in Q1 2022 (who can do what?)

Notes:
* Intro to the meeting, agenda, etc
    * See pdf of etherpad notes "2021-11-10_community-meetup_pad_standard-usage-scenarios" sent around before meetup.
* Presentation by student from Umwelt-Campus Birkenfeld on standard usage scenarios (SUS)
    * 30 loops needed,automation is mandatory
    * shows an example of the automation with Actiona
    * shows the automation in Actiona (lines that can be changed, time stamped)
    * actions are not required but can be documented to improve formatting and later on be able to know what was happening at that time
    * common errors depending on the automation software like: click-positions are changing, predicting durations wrong, downloads, pop-ups
    * shows lab setup
    * shows example of OSCAR report and table with summary for Blauer Engel
    * frequently used and well known actions should be used for the SUS
    * Comment: See https://seafile.rlp.net/f/8b1cb3bf6b014fa99466/
* Questions and discussion on tools
    * BE4FOSS: Kate and GCompris would be good software to test; suggestion to split up in 2 groups: one discussing tools that could be used and one discussing implementing SUS for applications; group decides to stay in one group to discuss tools together
    * SUS for Kate: https://invent.kde.org/-/snippets/1937
    * Discussion of XMLGUI, openQA, Squish, XNEE, PyAutoGUI, Atbswp
    * Comment: VMs or image recognition might however be more energy-hungry than the app under test itself
    * Comment: a11y interface sounds like a very interesting approach though
    * Comment: Kate is more C++/Qt Widgets, GCompris is full qml (so no kxmlgui)
    * Comment: a11y interface should also work with QML (unlike xmlgui)
* Next community meetup: Hands-on workshop (xdotool and bash)