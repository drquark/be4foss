<!--
SPDX-FileCopyrightText: 2021 KDE e.V. <https://ev.kde.org/>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

<span class="timestamp-wrapper"><span class="timestamp">&lt;2021-07-14 Wed&gt; </span></span> WORK IN PROGRESS: PLEASE SEND ANY COMMENTS, SUGGESTIONS, OR OTHER FEEDBACK TO joseph@kde.org


# Table of Contents

1.  [Laboratory Setup](#org47fc6ae)
2.  [Preparation](#org3bcbba2)
    1.  [System Under Test](#org65ba06d)
    2.  [Standard Usage Scenario](#orgfd1be0a)
3.  [Three Measurements: Baseline, Idle Mode, Standard Usage Scenario](#orgb6a90db)
    1.  [Baseline & Idle Mode](#orgcce5be7)
    2.  [Standard Usage Scenario](#org3f6f036)
4.  [Analysis Of The Results With OSCAR](#org151b131)
    1.  [Files Needed](#org2b66a2e)
    2.  [Preprocessing Data](#org697d456)
    3.  [Analysis With OSCAR](#orgfbb131f)

Here is the workflow with notes for measuring the energy consumption of interactive applications running locally<sup><a id="fnr.1" class="footref" href="#fn.1">1</a></sup> (e.g., mail clients, pdf readers), the data from which can be analyzed using the online tool OSCAR (Open source Software Consumption Analysis in R).

This workflow is a summary of the process described in detail in the following documents:

-   Mai, Franziska (2021) *Vergleichende Analyse und Bewertung von Betriebssystemen hinsichtlich ihrer Energieeffizienz* (German only),
-   Seiwert & Zaczyk (2021) [*Projektbericht: Ressourceneffiziente Softwaresysteme am Beispiel von KDE-Software*](https://invent.kde.org/cschumac/feep/-/blob/master/measurements/abschlussbericht-kmail-krita.pdf) (German only),
-   [OSCAR Manual](https://gitlab.umwelt-campus.de/y.becker/oscar-public/blob/master/OSCAR/static/OSCAR_Gesamt.pdf) (German only), and
-   Section 4.1 of Kern et al. (2018), *[Sustainable software products&#x2013;Towards assessment criteria for resource and energy efficiency](https://www.umwelt-campus.de/fileadmin/Umwelt-Campus/Greensoft/1-s2.0-S0167739X17314188-main.pdf)*.

Note that for eco-certification it is possible the certifier will require a different process than that described here.


<a id="org47fc6ae"></a>

# Laboratory Setup

Cornelius Schumacher has written about measurement tools in the [FOSS Energy Efficiency Project (FEEP) repository](https://invent.kde.org/cschumac/feep/-/blob/master/measurement_setup.md), including examples of software and hardware which can be used to set up a laboratory. See also Volker Krause's 2020 blog post on [Cheap Electric Power Measurement](https://volkerkrause.eu/2020/10/17/kde-cheap-power-measurement-tools.html).

The laboratory setup requires 1 power meter and 2 computers:

-   The Power Meter (**PM**) &#x2013; for measuring active power usage (e.g., [Gude Expert Power Control 1202 Series](https://www.gude.info/en/power-distribution/switched-metered-pdu/expert-power-control-1202-series.html) or [Janitza UMG 604](https://www.janitza.com/umg-604-pro.html)).
-   Computer 1: Data Aggregator & Evaluator (**DAE**) &#x2013; the computer for collecting measurements from the power meter (may additionally require specific software, e.g., Janitza's [GridVis Power Grid Monitoring Software](https://www.gridvis.com/gridvis-overview.html)).
-   Computer 2: Reference System &#x2013; the hardware for which the energy consumption of the system under test (**SUT**) is measured by the DAE/PM. The SUT includes the operating system (**OS**) and software installed for (i) testing the application under consideration, as well as (ii) emulating the standard usage scenario<sup><a id="fnr.2" class="footref" href="#fn.2">2</a></sup> and (iii) collecting the results. Note the following:
    -   For GNU/Linux systems (e.g., Ubuntu 18.04 or 20.04), the [Blauer Engel criteria (Section 1.1)](https://produktinfo.blauer-engel.de/uploads/criteriafile/en/DE-UZ%20215-202001-en-Criteria-2020-02-13.pdf) recommend Fujitsu computers as the reference system.
    -   For emulating activity in the standard usage scenario, a task automation tool such as  ["Actiona"](https://wiki.actiona.tools/doku.php?id=en:start) (GNU/Linux) is used.
    -   For collecting hardware performance data (e.g., processor utilization, RAM utilization, hard disk activity, network traffic), software such as [Collectl](https://sourceforge.net/projects/collectl/) (GNU/Linux) is used.

Achim Guldner has provided a Python script (GPLv3) to read out the data from the Gude Expert Power Control 1202 Series with SNMP: <https://gitlab.rlp.net/a.guldner/mobiles-messgerat>.

In Mai (2021), Seiwert & Zaczyk (2021), and the OSCAR Manual the net visualization software GridVis was used on the DAE to read out the data recorded by the Janitza UMG 604 power meter, which required Windows OS. Other power meters may work with GNU/Linux. Note that with the Janitza UMG 604 power meter it is possible also to download the power measurement data directly from the power meter, but it is recommended to monitor progress live with the second computer in order to ensure everything is proceeding smoothly.


<a id="org3bcbba2"></a>

# Preparation


<a id="org65ba06d"></a>

## System Under Test

The System Under Test (SUT) must be carefully prepared in order to reduce unrelated energy consumption and have a standardized configuration. This includes the following:

-   Overwriting the entire hard drive of the machine with a standardized OS.
-   Deactivating all possible background processes (automatic updates, backups, indexing, etc.).
-   Installing the necessary software (i.e., the application under consideration as well as the emulation and data collection software).


<a id="orgfd1be0a"></a>

## Standard Usage Scenario

Preparing the Standard Usage Scenario requires the following:

-   Identifying tasks users typically carry out when using the application under consideration.
-   Identifying functionalities which require high energy demand or high resource utilization.
-   Based on the above, scheduling a flow chart of individual actions and emulating these actions with a task automation tool (e.g., Actiona for GNU/Linux).
-   Note that between runs the cache should be fully cleared to start the next measurement on a clean OS.

An example standard usage scenario for KMail includes: searching for an email, writing a reply or forwarding the email, saving an attachment, deleting a folder in the mail client, etc. See these [Actiona scripts](https://invent.kde.org/cschumac/feep/-/tree/master/measurements) used to test KMail, Krita, and Okular for examples; see the OSCAR Manual for details about using WinAutomation (Windows).

Note: If the task automation tool uses pixel coordinates to store the position of the automated clicks (e.g., Actiona) and, moreover, the screen resolution of the computer used in preparation differs from that of the laboratory computer, all pixel coordinates will eventually have to be reset for the laboratory environment, resulting in the loss of time and energy; see Seiwert & Zaczyk (2021: p. 12) for details about their experience.


<a id="orgb6a90db"></a>

# Three Measurements: Baseline, Idle Mode, Standard Usage Scenario

These measurements are conducted in a laboratory. Some general comments:

-   For all measurements, the PM, reference system, and DAE must be turned on.
-   Times between the PM and DAE must be syncronized.
-   On the DAE confirm that the desired power outlet is read out (e.g., via the live graph created when using the GridVis program).
-   When using Collectl to collect performance load, ensure it is running in the console of the reference system; furthermore, it is recommended to check that the required csv file is also correctly generated.
-   Since each run of the usage scenarios results in changes to the standard operating system, the OSCAR Manual recommends creating an image (i.e., a copy of the operating system) which is installed prior to each measurement; since the process of reinstalling the OS for each run may significantly increase measurement times, clearing the cache between runs should also be possible.
-   All runs (Baseline, Idle Mode, Standard Usage Scenario) must be for the same length of time, based on the time needed to run the Standard Usage Scenario script.

Mai (2021: p. 15) recommends the following command for obtaining hardware performance data with Collectl:

$ `collectl -s cdmn -i1 -P --sep 59 -f /var/log/performanceMeasures.csv`

The specified options are:

`-s cdmn` = collect CDU, Disk, memory, and network data

`-i1` = sampling interval of 1 second

`-P` = output in plot format (separated data which consists of a header with one line per sampling interval)

`--sep 59` = semicolon separator for -P option

`-f /PATH/TO/FILE.csv` = save file at specified path


<a id="orgcce5be7"></a>

## Baseline & Idle Mode

Baseline (OS): To establish the baseline electrical power usage and hardware performance data of the system under test, a scenario in which no action is taken while the OS is running in the background is measured. 

Idle Mode (OS + application under consideration in idle mode): To establish the electrical power usage and hardware performance data of the application under consideration while idle, a scenario in which the application is opened on the OS but subsequently no action is taken is measured.

Important: the baseline and idle mode are run for the same time needed to carry out the standard usage scenario. In Seiwert & Zaczyk (2021) both measurements were repeated 10 times, taking about 70 minutes each (140 minutes in total). Since the power consumption for the baseline and idle scenario is relatively uniform, 10 repetitions for each is considered sufficient to obtain a representative sample.


<a id="org3f6f036"></a>

## Standard Usage Scenario

Standard Usage Scenario: To measure the electrical power usage and hardware performance data of the application under consideration in use, the standard usage scenario is run. In Seiwert and Zaczyk (2021), the measurement of the standard usage scenario was repeated 30 times, taking about 4 hours in total. This higher number of repetitions was necessary to obtain a representative sample, as the electrical power usage and performance data may vary across measurements.


<a id="org151b131"></a>

# Analysis Of The Results With OSCAR

OSCAR (Open source Software Consumption Analysis in R) ([source code](https://gitlab.rlp.net/a.guldner/OSCAR-public)) is a tool developed by the Umwelt-Campus Birkenfeld der Hochschule Trier for evaluating the results of the above measurements. At the project website there is the Oscar Manual with detailed instructions including screenshots on how to use OSCAR.


<a id="org2b66a2e"></a>

## Files Needed

The evaluation will begin once the following files are uploaded to the [OSCAR website](https://oscar.umwelt-campus.de/): (i) a [log file of actions](#orge434558) taken, (ii) the [electrical power usage](#orgfc7320d), and (iii) the [hardware performance data](#orgf834888). All files are .csv files with a semicolon separator, examples of which are provided below. Important: some preprocessing of the raw data may be necessary (e.g., performance data measured by Collectl; see [this section](#org81ebe69) for details).


### <a id="orge434558"></a> Log File Of Actions

A log file of actions taken will have the following format (OSCAR Manual: Section 3.1.1):

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />
</colgroup>
<tbody>
<tr>
<td class="org-left">timestamp;startTestrun</td>
</tr>


<tr>
<td class="org-left">timestamp;startAction;action1</td>
</tr>


<tr>
<td class="org-left">timestamp;stopAction</td>
</tr>


<tr>
<td class="org-left">timestamp;startAction;action2</td>
</tr>
</tbody>
</table>

Here is an example from the OSCAR Manual using WinAutomation:

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />
</colgroup>
<tbody>
<tr>
<td class="org-left">2018-01-12 17:13:46.680;startTestrun</td>
</tr>


<tr>
<td class="org-left">2018-01-12 17:13:46.774;startAction;OpenProgram</td>
</tr>


<tr>
<td class="org-left">2018-01-12 17:14:49.055;stopAction</td>
</tr>


<tr>
<td class="org-left">2018-01-12 17:14:49.718;startAction;LoadFile</td>
</tr>


<tr>
<td class="org-left">2018-01-12 17:15:59.804;stopAction</td>
</tr>


<tr>
<td class="org-left">2018-01-12 17:15:59.878;startAction;EditContents</td>
</tr>


<tr>
<td class="org-left">2018-01-12 17:23:26.184;stopAction</td>
</tr>


<tr>
<td class="org-left">2018-01-12 17:23:26.274;startAction;SaveFile</td>
</tr>


<tr>
<td class="org-left">2018-01-12 17:24:34.591;stopAction</td>
</tr>


<tr>
<td class="org-left">2018-01-12 17:24:26.274;startAction;CloseProgram</td>
</tr>


<tr>
<td class="org-left">2018-01-12 17:24:34.591;stopAction</td>
</tr>


<tr>
<td class="org-left">2018-01-12 17:25:16.635;stopTestrun</td>
</tr>
</tbody>
</table>


### <a id="orgfc7320d"></a> Electrical Power Usage

The electrical power usage will have the following format (OSCAR Manual: Section 3.1.2):

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />
</colgroup>
<tbody>
<tr>
<td class="org-left">timestamp;value1</td>
</tr>


<tr>
<td class="org-left">timestamp;value2</td>
</tr>


<tr>
<td class="org-left">timestamp;value3</td>
</tr>


<tr>
<td class="org-left">timestamp;value4</td>
</tr>
</tbody>
</table>

Note: (i) the timestamp increases in one-second increments and (ii) *value* stands for 'averaged measured value per second in watts'.

Below is an example with header from the OSCAR Manual (measurements are from the Janitza UMG 604 power meter exported on the DAE using the GridVis interface):

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />
</colgroup>
<tbody>
<tr>
<td class="org-left">Row-Number;Time;Valueavg[W];Value-min[W];Value-max[W];</td>
</tr>


<tr>
<td class="org-left">1;18.01.12 17:00:01;78,573;78,281;78,982;</td>
</tr>


<tr>
<td class="org-left">2;18.01.12 17:00:02;78,611;78,390;78,972;</td>
</tr>


<tr>
<td class="org-left">3;18.01.12 17:00:03;78,556;78,353;78,887;</td>
</tr>


<tr>
<td class="org-left">4;18.01.12 17:00:04;78,589;78,391;78,882;</td>
</tr>


<tr>
<td class="org-left">5;18.01.12 17:00:05;78,546;78,288;79,057;</td>
</tr>


<tr>
<td class="org-left">6;18.01.12 17:00:06;78,580;78,252;78,941;</td>
</tr>


<tr>
<td class="org-left">7;18.01.12 17:00:07;80,712;78,330;87,391;</td>
</tr>


<tr>
<td class="org-left">8;18.01.12 17:00:08;78,545;78,275;79,202;</td>
</tr>
</tbody>
</table>

Note: The columns above containing min and max are ignored by OSCAR.


### <a id="orgf834888"></a> Performance Data (Raw)

The hardware performance data will have the following format (OSCAR Manual: Section 3.1.3):

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />
</colgroup>
<tbody>
<tr>
<td class="org-left">timestamp;value1</td>
</tr>


<tr>
<td class="org-left">timestamp;value2</td>
</tr>


<tr>
<td class="org-left">timestamp;value3</td>
</tr>


<tr>
<td class="org-left">timestamp;value4</td>
</tr>
</tbody>
</table>

Note: The timestamp again increases in one-second increments.

Below is an example of the preprocessed results from Collectl running on Ubuntu 16.04 LTS (OSCAR Manual). The measurements that need to be specified in OSCAR are: [CPU]Totl = Processor; [MEM]Used = Main memory - used kilobytes; [NET]RxKBTot = Network - Kilobytes received/s; [NET]TxKBTot = Network - Kilobytes sent/s; [DSK]ReadKBTot = Disk - Kilobytes read/s; and [DSK]WriteKBTot = Disk - kilobytes written/s.

See below for notes about preprocessing Collectl results for use with OSCAR. 

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />
</colgroup>
<tbody>
<tr>
<td class="org-left">Date Time;[CPU]User%;[CPU]Nice%;[CPU]Sys%;[CPU]Wait%;[CPU]Irq%;[CPU]Soft%;[CPU]Steal%;[CPU]Idle%;[CPU]Totl%;[CPU]Guest%;[CPU]GuestN%;[CPU]Intrpt/sec;[CPU]Ctx/sec;[CPU]Proc/sec;[CPU]ProcQue;[CPU]ProcRun;[CPU]L-Avg1;[CPU]L-Avg5;[CPU]L-Avg15;[CPU]RunTot;[CPU]BlkTot;[MEM]Tot;[MEM]Used;[MEM]Free;[MEM]Shared;[MEM]Buf;[MEM]Cached;[MEM]Slab;[MEM]Map;[MEM]Anon;[MEM]AnonH;[MEM]Commit;[MEM]Locked;[MEM]SwapTot;[MEM]SwapUsed;[MEM]SwapFree;[MEM]SwapIn;[MEM]SwapOut;[MEM]Dirty;[MEM]Clean;[MEM]Laundry;[MEM]Inactive;[MEM]PageIn;[MEM]PageOut;[MEM]PageFaults;[MEM]PageMajFaults;[MEM]HugeTotal;[MEM]HugeFree;[MEM]HugeRsvd;[MEM]SUnreclaim;[NET]RxPktTot;[NET]TxPktTot;[NET]RxKBTot;[NET]TxKBTot;[NET]RxCmpTot;[NET]RxMltTot;[NET]TxCmpTot;[NET]RxErrsTot;[NET]TxErrsTot</td>
</tr>


<tr>
<td class="org-left">2018/01/12 16:39:01;5;0;1;0;0;0;0;94;6;0;0;467;1924;0;934;0;0.11;0.33;0.52;0;0;3907988;2614772;1293216;78176;41244;465792;86972;340884;1786576;112640;12515016;80;3857404;2119904;1737500;4108;0;508;0;0;211740;4;0;33;1;0;0;0;45296;4;0;1;0;0;1;0;0;0</td>
</tr>


<tr>
<td class="org-left">2018/01/12 16:39:02;2;0;1;0;0;0;0;97;3;0;0;374;1726;0;934;0;0.11;0.33;0.52;0;0;3907988;2614896;1293092;78176;41252;465784;86972;340884;1786676;112640;12515016;80;3857404;2119904;1737500;0;0;508;0;0;211768;0;28;82;0;0;0;0;45296;4;4;1;1;0;0;0;0;0</td>
</tr>


<tr>
<td class="org-left">2018/01/12 16:39:03;2;0;0;0;0;0;0;98;2;0;0;363;1710;0;934;0;0.10;0.32;0.52;0;0;3907988;2614896;1293092;78176;41252;465792;86972;340884;1786756;112640;12515016;80;3857404;2119904;1737500;0;0;528;0;0;211768;0;12;16;0;0;0;0;45296;1;0;0;0;0;0;0;0;0</td>
</tr>


<tr>
<td class="org-left">2018/01/12 16:39:04;1;0;0;0;0;0;0;98;2;0;0;379;1766;0;933;0;0.10;0.32;0.52;0;0;3907988;2614896;1293092;78176;41252;465792;86972;340884;1786548;112640;12515016;80;3857404;2119904;1737500;0;0;516;0;0;211768;0;0;87;0;0;0;0;45296;2;0;1;0;0;0;0;0;0</td>
</tr>


<tr>
<td class="org-left">2018/01/12 16:39:05;2;0;1;0;0;0;0;98;2;0;0;376;1718;0;930;0;0.10;0.32;0.52;0;0;3907988;2612972;1295016;78176;41252;465792;86972;340884;1784684;110592;12515016;80;3857404;2119904;1737500;0;0;516;0;0;211764;0;0;59;0;0;0;0;45296;1;0;0;0;0;0;0;0;0</td>
</tr>


<tr>
<td class="org-left">2018/01/12 16:39:06;5;0;1;0;0;0;0;93;6;0;0;784;3041;0;930;0;0.10;0.32;0.52;0;0;3907988;2612972;1295016;78176;41252;465792;86972;340884;1785104;110592;12515016;80;3857404;2119748;1737656;221184;0;736;0;0;211764;216;0;713;33;0;0;0;45296;14;9;2;1;0;0;0;0;0</td>
</tr>


<tr>
<td class="org-left">2018/01/12 16:39:07;2;0;1;0;0;0;0;97;3;0;0;687;2659;0;930;0;0.10;0.32;0.52;0;0;3907988;2613048;1294940;78176;41252;465792;86972;340884;1785160;110592;12515016;80;3857404;2119748;1737656;0;0;732;0;0;211764;0;0;180;0;0;0;0;45296;5;5;1;1;0;0;0;0;0</td>
</tr>


<tr>
<td class="org-left">2018/01/12 16:39:08;2;0;1;0;0;0;0;98;2;0;0;409;1973;0;930;0;0.10;0.32;0.51;0;0;3907988;2612972;1295016;78176;41252;465792;86972;340884;1785216;110592;12515016;80;3857404;2119748;1737656;0;0;796;0;0;211764;0;0;20;0;0;0;0;45296;10;4;2;1;0;0;0;0;0</td>
</tr>
</tbody>
</table>


<a id="org697d456"></a>

## <a id="org81ebe69"></a> Preprocessing Data

Before using OSCAR some preprocessing of the data may be necessary. For example, when using Collectl for performance data of the SUT it is necessary to do the following before uploading the data to OSCAR (see Seiwert & Zaczyk 2021: p. 13 for details; see also Appendix A 2 on p. 46 for a Python script to automate some of these tasks):

-   All information above the header row can be removed.
-   Remove all # characters from the file.
-   In the first column the semicolon between the data/time and data must be removed or the date and time will be interpreted as two separate columns.
-   Moreover, the date should have a character inserted between YYYYMMDD, e.g., YYYY/MM/DD. Whatever character is used must be specified in OSCAR.
-   The file must be saved in csv format.


<a id="orgfbb131f"></a>

## Analysis With OSCAR


### Uploading The Results

Once the necessary files have been preprocessed, the evaluation of the baseline, idle mode, and standard usage scenario can begin. Note that the baseline measurements are uploaded separately from the idle mode/standard usage scenario measurements. When uploading the idle mode measurements, for *Art der Messung* ('Type of Measurement') select *Leerlauf* ('Idle Mode'); when uploading the standard usage scenario measurements select *Nutzungsszenario* ('Use Scenario'). For each measurement, the log file of actions taken, the electrical power usage, and the hardware performance data are uploaded.

In the OSCAR interface, note the following:

-   The duration of the individual measurements (for instance, the duration of a measurement in seconds) must be specified.
-   The formatting of the data must be specified.
-   Only the semicolon must be specified as separator.
-   For the performance output of the hardware, it is necessary to select the columns that are relevant for evaluation.
-   As a last step, the correct formatting of the time stamp must be specified for each of the uploaded files.

An example of time stamp formatting specified in OSCAR is as follows:

*Aktionen*

%Y-%m-%d %H:%M:%OS

*Elektrische Leistung*

%d.%m.%y %H:%M:%OS

*Hardware-Auslastung*

%Y/%m/%d %H:%M:%OS

Note that in the above example the character used for the date in the preprocessing of the Collectl hardware performance data (*Hardware-Auslastung*) is a slash, i.e., YYYY/MM/DD.

For more details, including screenshots, see the OSCAR Manual (Section 3.3).


### Downloading The Reports

After completing the above, the report can be generated and downloaded, resulting in two documents: one is a report for the baseline and idle mode, and the other is a report for the baseline and standard usage scenario.


# Footnotes

<sup><a id="fn.1" href="#fnr.1">1</a></sup> For measuring the energy consumption and hardware performance of software running locally it is not necessary to also measure transmission and remote systems.

<sup><a id="fn.2" href="#fnr.2">2</a></sup> In Kern et al. (2018) they describe a setup using 3 computers, with the standard usage scenario emulation generated on a computer independent of the SUT.
