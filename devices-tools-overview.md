
# Table of Contents

1.  [Brief Overview](#orgfc4cd92)
2.  [Detailed Overview (With References & Notes)](#org7095296)
    1.  [Software Products (Measured)](#org1567ceb)
    2.  [Reference Systems](#org9f8dbc0)
    3.  [Emulation Tools](#org370db29)
        1.  [Note: Usage Scenarios](#org4ffba7f)
    4.  [Power Meters & Software](#org3125ebd)
    5.  [Hardware Performance](#org4460c21)
    6.  [Data Analysis](#org7f61923)
        1.  [Data Files](#org665cafe)
        2.  [Analysis Tools](#org5c7f552)
3.  [References](#orgc55b762)



<a id="orgfc4cd92"></a>

# Brief Overview

I will put FOSS-compatible options at the top of the list.

Software Products (Measured):

-   Krita
-   KMail
-   Okular
-   Ubuntu 20.04 LTS
-   Windows 10

Reference Systems:

-   Fujitsu Desktop Computer Esprimo P920 / P956 / P957 / P958 (Blauer Engel recommended)
-   Hyndai Pentio H-Series AIO MT B85
-   Mac mini "Core i5" 2.6, 3.0 (Blauer Engel recommended)

Available Emulation Tools (see David Hurka's [list](<https://invent.kde.org/teams/eco/feep/-/tree/master/tools/presentation_Visual_Workflow_Automation_Tools>)):

-   [Actiona](<https://actiona.tools/>) (GPL v3.0)
-   [xdotool](<https://www.semicomplete.com/projects/xdotool>) (New BSD)
-   [KXmlGui](<https://github.com/KDE/kxmlgui>) (GOL v.2.0, among others)
-   [Xnee](<https://savannah.gnu.org/projects/xnee/>) (GPL v2.0)
-   [PyAutoGUI](<https://pypi.org/project/PyAutoGUI/>) (BSD)
-   [Atbswp / Automate the boring stuff with Python](<https://github.com/rmpr/atbswp>) (GPL v3.0)
-   [Sikulix](<https://sikulix.github.io/>) (MIT)
-   [PuloversMacroCreator](<https://macrocreator.com>) (GPL v3.0)
-   [Squish](<https://www.froglogic.com/squish/>) (Proprietary)
-   [Eggplant Functional](<https://docs.eggplantsoftware.com/ePF/gettingstarted/epf-getting-started-eggplant-functional.htm>) (Proprietary)
-   [WinAutomation / Microsoft Power Automate Desktop](<https://docs.microsoft.com/en-us/power-automate/desktop-flows/>) (Proprietary)

Power Meters & Software:

-   GUDE Expert Power Control 1202
    -   [Python script](<https://gitlab.rlp.net/a.guldner/mobiles-messgerat>) from Achim Guldner (GPL v3.0)
-   Gosund SP111 (inexpensive power plug) with [Tasmota firmware](<https://tasmota.github.io/docs>) (GPL v3.0) 
    -   [Kst](<https://github.com/Kst-plot/kst>) (GPL v2.0, KDE software)
    -   See [Volker Krause's blog post](<https://volkerkrause.eu/2020/10/17/kde-cheap-power-measurement-tools.html>)
-   Janitza UMG 604
    -   GridVis (Proprietary)

Hardware Performance:

-   [Collectl](<https://sourceforge.net/projects/collectl/>) (measurement data in KiB) (GPLv2/Artistic License)
-   Windows Performance Monitor (measurement data in Bytes) (Proprietary)

Data Analysis:

-   Data files
    -   .csv format
-   Analysis tools
    -   [OSCAR](<https://gitlab.rlp.net/a.guldner/OSCAR-public>) (Open source Software Consumption Analysis in R) ([source code](<https://gitlab.rlp.net/a.guldner/OSCAR-public>)) (GPL v3.0)


<a id="org7095296"></a>

# Detailed Overview (With References & Notes)


<a id="org1567ceb"></a>

## Software Products (Measured)

-   Windows 10 vs. Ubuntu 20.04 (Mai 2021)
    -   Various applications were installed and used (VLC, Spotify, FireFox) in the usage scenario; see pp. 22-23 for the SUS
    -   See p. 38 for issues related to Actiona emulation with Windows
    -   Note: The thesis has not been published given the potentially sensitive nature of the work

-   Krita 4.4.2 (Seiwert & Zaczyk 2021)
    -   "Um das Szenario dennoch möglichst realistisch zu gestalten, wurden Mauszeigerpfade (d.h. Mausbewegungen, bei denen in Actiona nicht zu einer bestimmten Koordinate gesprungen wird, sondern welche durch viele einzelne Koordinaten beschrieben sind) mit einem Grafiktablett aufgezeichnet." (p. 23)
    -   SUS "30-mal hintereinander ausgeführt, Leerlauf und Grundauslastung jeweils zehnmal" (p. 27)
    -   SUS length: "502 Sekunden bzw. acht Minuten und 22 Sekunden" (p. 25)

-   KMail 5.16.3 (Seiwert & Zaczyk 2021)
    -   See also Blauer Engel application Annex 2: <https://invent.kde.org/teams/eco/blue-angel-application/-/blob/master/applications/kmail/de-uz-215-eng-annex-2-kmail.xlsx>
    -   "Das Mailkonto wurde im Vorhinein mit ca. 900 Nachrichten präpariert, darunter 500 HTML E-Mails, 300 Plaintext E-Mails und weitere 100 Plaintext E-Mails mit Anhang" (p. 10)
    -   "Baseline beanspruchte ca. 70 Minuten" (p. 12)
    -   Standardnutzungsszenarios: "ca. 4 Stunden" (p. 12)
    -   See p. 20 for a summary of issues related to SUS

-   Okular 1.9.3 ([Blauer Engel application](<https://invent.kde.org/teams/eco/blue-angel-application/-/tree/master/applications/okular>))


<a id="org9f8dbc0"></a>

## Reference Systems

-   Fujitsu Desktop Computer Esprimo P958 (Ubuntu 20.04) (Seiwert & Zaczyk 2021)
    -   "Intel i5-8500 Prozessor mit 6 Kernen und einer Taktfrequenz von 3,0 GHz, 16 GB RAM, eine SSD Festplatte mit 512 GB Speicher und einer Intel UHD Graphics 630 Grafikkarte" (p. 7)

-   Hyndai Pentio H-Series AIO MT B85 (Windows 10 vs. Ubuntu 20.04) (Mai 2021)
    -   "Hersteller: IT Media Consult AG, Architektur: 64-Bit, Mainboard: MSI CSM-B85M-E45, Prozessor: Intel® CoreTM i5-4460 4 x 3,20 GHz, Cache (L1/L2/L3): 256 KB/1 MB/6 MB, RAM: 8 GB DDR3, Grafikkarte: Intel® HD-Grafik 4600, Festplatte: 256 GB SSD, Netzwerk: Gigabit Ethernet Controller, Netzteil: XILENCE XP420 Passive PFC 420 W" (p. 13)


<a id="org370db29"></a>

## Emulation Tools

-   Actiona (Mai 2021, Seiwert & Zaczyk 2021)
    -   See Seiwert & Zaczyk (2020-2021) p. 12 for issues related to monitor sizes
    -   See Mai (2021) p. 19 for notes related to using Actiona on Linux and Windows
    -   See Mai (2021) pp. 24&#x2013;27 for notes related to timestamps and pre-configuration of applications
-   xdotool (Fella 2022)


<a id="org4ffba7f"></a>

### Note: Usage Scenarios

-   ca. 30 repetitions for SUS, ca. 10 for Baseline/Idle (Mai 2021, Seiwert & Zaczyk 2021)


<a id="org3125ebd"></a>

## Power Meters & Software

-   Janitza UMG 604, GridVis + Windows (Mai 2021, Seiwert & Zaczyk 2021)


<a id="org4460c21"></a>

## Hardware Performance

-   Collectl (Mai 2021, Seiwert & Zaczyk 2021)
    -   Example command from Mai (2021): \`collectl -s cdmn -i1 -P &#x2013;sep 59 -f /var/log/Beispielname.csv\`
    -   Measurement data in KiB, i.e., \`coarser measurements' than Windows Performance Monitor (Mai 2021: p. 61)
-   Windows Performance Monitor ('Leistungsüberwachung') (Mai 2021; see pp. 14&#x2013;15)
    -   Measurement data in Bytes, i.e., more fine-grained measurements than Collectl (Mai 2021: p. 61)


<a id="org7f61923"></a>

## Data Analysis


<a id="org665cafe"></a>

### Data Files

-   .csv format (Energy/Hardware: Mai 2021, Seiwert & Zaczyk 2021)


<a id="org5c7f552"></a>

### Analysis Tools

-   OSCAR (Mai 2021, Seiwert & Zaczyk 2021)
    -   See Seiwert & Zaczyk (2021) pp. 13&#x2013;14, p. 46 for data preprocessing and Python script


<a id="orgc55b762"></a>

# References

Fella, Nicolas 2022. Running title: Continuous Energy Consumption Testing for Desktop Applications. M. Sc. Thesis, Universität Würzburg.

Mai, Franziska 2021. Vergleichende Analyse und Bewertung von Betriebssystemen hinsichtlich ihrer Energieeffizienz. B. Sc. Thesis, Umwelt Campus.

Seiwert, Ina & Zaczyk, Melissa 2021. Projektbericht: Ressourceneffiziente Softwaresysteme am Beispiel von KDE-Software. B. Sc. Thesis, Umwelt Campus, WS 2020-2021. <https://invent.kde.org/teams/eco/feep/-/blob/master/measurements/abschlussbericht-kmail-krita.pdf>

