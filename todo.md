<!--
SPDX-FileCopyrightText: 2021 KDE e.V. <https://ev.kde.org/>

SPDX-License-Identifier: CC-BY-SA-4.0
-->


# Table of Contents

1.  [`Roadmap`](#org596649e)
2.  [`TODOs`](#org809c100)
    1.  [Content](#org7f1dba7)
    2.  [Events, Workshops, Conferences](#org5978246)
    3.  [Other](#org24f0194)
3.  [`Project Goals`](#org6e1a795)

This document points to possible areas of work for the Blauer Engel For FOSS (BE4FOSS) project. The Roadmap below lists planned, high-priority tasks and events. You can join the discussions and contribute to their implementation at the [BE4FOSS GitLab repository](https://invent.kde.org/joseph/be4foss). Please check the TODO section below.


<a id="org596649e"></a>

# `Roadmap`

The main activities include organizing developer meetings, participating in conferences and other community events, and marketing activities.

**Phase 1** (July 2021&#x2013;March 2022)

-   July&#x2013;September 2021
    -   Project familiarization
    -   Detailed conception Phase 1
-   October&#x2013;December 2021
    -   Developer Meeting 1
    -   Creation of the website
-   January&#x2013;March 2022
    -   Developer Meeting 2
    -   Publication of the website

**Phase 2** (April 2022&#x2013;March 2023)

-   April&#x2013;June 2022
    -   Detailed conception Phase 2
    -   Creation of sustainable software manual
-   July&#x2013;September 2022
    -   Developer Meeting 3
    -   Publication of sustainable software manual
-   October&#x2013;December 2022
    -   Conferences
    -   Marketing
-   January&#x2013;March 2023
    -   Developer Meeting 4
    -   Write final report


<a id="org809c100"></a>

# `TODOs`


<a id="org7f1dba7"></a>

## Content

-   Project familiarization
    -   Write up workflow for measuring and analysis of resource efficiency
-   Plan content for website
-   Contact/outreach with FOSS community


<a id="org5978246"></a>

## Events, Workshops, Conferences

-   Plan Developer Meeting 1
    -   1-2 day event with 10-15 people
    -   Online?, who?, what?, when?
-   Conferences
    -   Akademy
    -   FOSDEM
    -   Linux App Summit
    -   CCC
    -   DebConf


<a id="org24f0194"></a>

## Other

-   Professional designer for website


<a id="org6e1a795"></a>

# `Project Goals`

Project results include the following:

-   Manual "Application of the Blauer Engel Criteria to Free Software" (working title)
-   Summary of community feedback for the further development of the Blauer Engel criteria and resource efficiency for software products
-   Website "Sustainable Development of Free Software" (working title)
-   Presentations on the project for conferences and meetups
-   Articles about the project for news sites and press releases
-   Social media content
-   Final report

