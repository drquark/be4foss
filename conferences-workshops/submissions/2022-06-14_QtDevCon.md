# Why Energy Consumption Of Digitization Matters (And How To Eco-Certify Your Software)

## Abstract

It is obvious that energy conservation and energy efficiency in software engineering mean fewer shared resources are required to keep our digital society running. Oft overlooked is that Free Software is well-suited to achieve both!

Free and Open-Source Software guarantees user autonomy through the four freedoms. This autonomy permits users to install only what they need or bypass bloatware, reducing background processes and thus conserving energy. This autonomy also means users may choose to continue using aging hardware while keeping the software up-to-date — reducing global CO2 emissions as a result by avoiding the unnecessary production and shipment of new devices. Moreover, Free and Open-Source Software guarantees transparency: this has always meant that anyone may inspect and learn from how software runs; today, this transparency can be extended to include software’s energy demands when in use. By providing transparency in energy consumption, developers can make their software more efficient and users can make more informed choices.

In this talk I will present how the FOSS values of autonomy and transparency enable one to directly influence the factors determining software sustainability. Additionally, I will announce the new FOSS measurement lab at KDAB Berlin and present the three steps to Blauer Engel eco-certification.

## About the speaker

Joseph P. De Veaugh-Geiss (he/him) is the project and community manager of KDE e.V.’s “Blauer Engel 4 FOSS” project. He supports the project by collecting and spreading information about Blauer Engel eco-certification and resource efficiency as it relates to Free Software.

