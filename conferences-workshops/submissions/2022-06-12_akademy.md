# Panel: Achievements, Impact, and To-Dos For KDE Eco

Thanks to our core values of transparency and user autonomy, Free Software has an edge to become the most efficient and sustainable software. This observation has been the motivation behind the KDE Eco project since its start in 2021. We aim both to quantify and drive down the energy consumption of KDE/Free Software, as well as to cultivate a culture of sustainability within the FOSS community. In this panel discussion we will present the (i) *achievements*, (ii) *impact*, and (iii) *to-dos* of the KDE Eco initiative. Specifically, we will address the following:

 * **What are the *achievements* of KDE Eco so far?** These include Okular receiving the Blue Angel eco-label, the official environmental label awarded by the German government; they also include research by KDE community members into methods of energy consumption measurements, setting up a community lab at KDAB Berlin, and developing tools for measuring software, among others.

 * **What has been the *impact* of the KDE Eco initiative?** From the developer side, this includes discussion of efficiency-related bug reports / MR requests and efficiency changes in software design; from the user side, this includes information related to community responses in (social) media as well as quantifying engagement and other metrics.

 * **What are the *to-dos* going forward?** Here we will present our ideas and wish lists as well as elicit input from the broader KDE community.

# Bio

Joseph De Veaugh-Geiss is the project and community manager of KDE e.V.’s “Blauer Engel 4 FOSS” project, part of the KDE Eco initiative.

Nicolas Fella wrote a master's thesis on the topic of energy consumption measurements in software and is an active contributor to KDE and the KDE Eco project.

Karanjot Singh was the Season of KDE 2022 student working on preparing usage scenarios to emulate user behavior when measuring software's energy demands.